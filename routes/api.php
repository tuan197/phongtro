<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/room/detail/{id}','RoomController@getDetail')->name('getDetail');

Route::group(['prefix' => 'room'], function () {
    Route::get('/get-room-lastest','HomeController@index');
    Route::get('/get-room-top','RoomController@getRoomTop');
    Route::get('/dangphong','RoomController@getDangPhong')->middleware('checkcreateroom');
    Route::post('/dangphong','RoomController@create')->middleware('checkcreateroom');
    Route::get('/filter','RoomController@getTemFilter');
    Route::post('/filter-room','RoomController@postFilter')->name('post.filter');
    Route::get('/search','RoomController@searchRoom')->name('search.room');
    Route::get('/map-room','RoomController@getRoomAround')->name("map.room");
    Route::get('/manager-room','RoomController@getTemManager')->middleware('auth');
    Route::post('/comment-room/{id}','RoomController@commentRoom')->name('comment.room');
    Route::get('/unpublic-room/{id}','RoomController@unPublicRoom')->middleware('auth');
    Route::get('/public-room/{id}','RoomController@publicRoom')->middleware('auth');
    Route::get('/edit-room/{id}','RoomController@editRoom')->middleware('auth')->name("edit.room");
    Route::post('/post-edit-room/{id}','RoomController@postEditRoom')->middleware('auth')->name("post.edit.room");
    Route::get('/delete-room/{id}','RoomController@deleteRoomByUser')->middleware('auth')->name("delete.room");
    Route::get('/get-room-by-province/{id}','RoomController@getRoomByProvince')->name('get.room.province');
    Route::post('/report-room','RoomController@rePortRoom');
    Route::get('/delete-image/{id}/{path}','RoomController@deleteImage');
});
Route::group(['prefix' => 'post'], function () {
    Route::get('/getTempWrite','PostController@getTempWrite')->name('post.getTempWrite');
    Route::post('/create-post','PostController@createPost')->name('create.post');
    Route::get('/get-post','PostController@getPostNew')->name('get.post.new');
    Route::get('/get-post-top-view','PostController@getPostTopView');
    Route::get('/post-detail/{id}','PostController@getDetailPost')->name('post.detail');
    Route::get('/manager-post','PostController@getPostByAuthor');
    Route::get('/delete/{id}','PostController@deletePost');
    Route::get('/get-edit/{id}','PostController@getEditPost');
    Route::post('/update-post/{id}','PostController@updatePost');
    Route::get('/get-new-post','PostController@getNewPost');
});
