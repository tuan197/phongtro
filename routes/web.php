<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//Route::get('/', 'RoomController@index')->name('home');
Route::get('{any}', function () {
    return view('welcome');
})->where('any','.*');

// Route::get('/room/detail/{id}','RoomController@getDetail')->name('getDetail');
// Route::get('/404','HomeController@get404');
// //Route::get('/','HomeController@index');
// Route::group(['prefix' => 'user'], function () {
//     Route::get('/register','UserController@register')->name('user.registerview');
//     Route::post('/postRegister','UserController@postRegister')->name('user.postRegister');
//     // login user
//     Route::get('/login','UserController@getLogin')->name('user.getLogin');
//     Route::post('/login','UserController@postLogin')->name('user.postLogin');
//     Route::get('/reset-password','UserController@getTemReset')->name('user.getReset');
//     Route::post('/reset','UserController@resetPass')->name('reset');

//     Route::get('/dangtin','UserController@getDangTin')->middleware('checkcreateroom')->name('user.dangtin');
//     Route::get('/updateAdress','UserController@getUpdateAdress')->middleware('auth');
//     Route::post('/update-info','UserController@postUpdateInfo')->name('post.address');
//     Route::post('/newPass','UserController@updateNewPass')->name('user.postNewPass');
//     Route::get('/sendReset/{email}/{token}','UserController@checkTokenReset');
//     Route::post('update','UserController@updateInfo');  
//     Route::get('get-info','UserController@getInfoMember');
//     Route::get('get-user/{id}','UserController@getUser');
//     Route::get('delete/{id}','UserController@deleteMemberRoom')->middleware('auth');
//     Route::get('logout','UserController@logout');
//     // Manager member room
//     Route::group(['prefix' => 'manager'], function () {
//         Route::get('/list','UserController@getListMemberOfMotel');
//         Route::post('/delete','UserController@deleteMemberOfMotel');
//         Route::post('/add','UserController@addMemberOfMotel');
//         Route::post('/updatetype','UserController@updateMemberOfMotel');
//     });
// });
// //Room
// Route::group(['prefix' => 'post'], function () {
//     Route::get('/getTempWrite','PostController@getTempWrite')->name('post.getTempWrite');
//     Route::post('/create-post','PostController@createPost')->name('create.post');
//     Route::get('/get-post','PostController@getPostNew')->name('get.post.new');
//     Route::get('/post-detail/{id}','PostController@getDetailPost')->name('post.detail');
//     Route::get('/manager-post','PostController@getPostByAuthor');
//     Route::get('/delete/{id}','PostController@deletePost');
//     Route::get('/get-edit/{id}','PostController@getEditPost');
//     Route::post('/update-post/{id}','PostController@updatePost');
// });

// Route::group(['prefix' => 'room'], function () {
//     Route::get('/dangphong','RoomController@getDangPhong')->middleware('checkcreateroom');
//     Route::post('/dangphong','RoomController@create')->middleware('checkcreateroom');
//     Route::get('/filter','RoomController@getTemFilter');
//     Route::post('/filter-room','RoomController@postFilter')->name('post.filter');
//     Route::get('/search','RoomController@searchRoom')->name('search.room');
//     Route::get('/map-room','RoomController@getRoomAround')->name("map.room");
//     Route::get('/manager-room','RoomController@getTemManager')->middleware('auth');
//     Route::post('/comment-room/{id}','RoomController@commentRoom')->name('comment.room');
//     Route::get('/unpublic-room/{id}','RoomController@unPublicRoom')->middleware('auth');
//     Route::get('/public-room/{id}','RoomController@publicRoom')->middleware('auth');
//     Route::get('/edit-room/{id}','RoomController@editRoom')->middleware('auth')->name("edit.room");
//     Route::post('/post-edit-room/{id}','RoomController@postEditRoom')->middleware('auth')->name("post.edit.room");
//     Route::get('/delete-room/{id}','RoomController@deleteRoomByUser')->middleware('auth')->name("delete.room");
//     Route::get('/get-room-by-province/{id}','RoomController@getRoomByProvince')->name('get.room.province');
//     Route::post('/report-room','RoomController@rePortRoom');
//     Route::get('/delete-image/{id}/{path}','RoomController@deleteImage');
    
// });

// Route::get('/admin-login','AdminController@getLogin')->name('admin.login');
// Route::post('/post-login','AdminController@postLogin');


// Route::group(['prefix' => 'admin','middleware' => 'check.auth'], function () {//
//     Route::get('/home','AdminController@getHomePage');
    
//     Route::group(['prefix' => 'user'], function () {
//         Route::get('/list','UserController@getAllUser');
//         Route::post('/update','UserController@updateLevel');//->middleware('check.auth');
//         Route::post('/delete','UserController@deleteUser');
//     });
//     Route::group(['prefix' => 'room'], function () {
//         Route::get('/list','RoomController@getAllRoom');
//         Route::get('/detail/{id}','RoomController@getRoomDetail');
//         Route::post('/delete','RoomController@deleteRoom');
//         Route::post('/close-room','RoomController@closeRoom');
//         Route::get('/new-room','RoomController@getRoomNotPublic');
//         Route::get('/public-room/{id}','RoomController@adminConfirmRoom');
//         Route::get('/block-room/{id}','RoomController@closeRoomAdmin');
//         Route::get('/get-report','RoomController@getReport');
//         Route::get('/detail-report-room/{id}/{room_id}','RoomController@detailReport');
//         Route::get('list-block-room','RoomController@getListRoomBlock');
//         Route::get('undoroom/{id}','RoomController@restoreRoom');
//     });

//     Route::group(['prefix' => 'province'], function () {
//         Route::post('/add','admin\ProvinceController@addProvince');
//         Route::get('/get-province','admin\ProvinceController@getProvince');
//         Route::post('/update','admin\ProvinceController@updateProvince');
//         Route::post('/delete','admin\ProvinceController@deleteProvince');
//     });
//     Route::group(['prefix' => 'district'], function () {
//         Route::post('/add','admin\ProvinceController@addDistrict');
//         Route::post('/update','admin\ProvinceController@updateDistrict');
//         Route::post('/delete','admin\ProvinceController@deleteDistrict');
//     });
//     Route::group(['prefix' => 'ward'], function () {
//         Route::post('/add','admin\ProvinceController@addWard');
//         Route::post('/update','admin\ProvinceController@updateWard');
//         Route::post('/delete','admin\ProvinceController@deleteWard');
//     });

//     Route::group(['prefix' => 'post'], function () {
//         Route::get('/list','PostController@getAllPost');
//         Route::get('/create','PostController@getCreatePost');
//         Route::post('/post-create','PostController@postCreate');
//         Route::get('/delete/{id}','PostController@deletePostAdmin');
//         Route::get('/detail/{id}','PostController@getDetailPostAdmin');
//     });
// });

// Route::get('/test-map',function(){
//     return view('page.map');
// });