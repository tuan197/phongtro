<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description');
            $table->integer('price');
            $table->integer('area');
            $table->integer('count_view')->default(0);
            $table->integer('electricity_price');
            $table->integer('water_price');
            $table->json('utility');
            $table->integer('accept');
            $table->json('latlng');
            $table->string('address');
            $table->unsignedBigInteger('ward_id');
            $table->json('image');
            $table->unsignedBigInteger('user_id');
            $table->integer('star')->default(1);
            $table->boolean('public')->nullable()->default(true);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ward_id')->references('id')->on('ward')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
