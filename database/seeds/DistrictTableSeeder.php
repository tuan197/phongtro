<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('district')->insert([
            'name' => "THành phố Huế",
            'slug' => "tp-hue",
            'province_id'=>5
        ],
        [
            'name' => "Hương Thủy",
            'slug' => "huong-thuy",
            'province_id'=>5
        ],
        [
            'name' => "Hương Trà",
            'slug' => "huong-tra",
            'province_id'=>5
        ],
        [
            'name' => "Nam Đông",
            'slug' => "nam-dong",
            'province_id'=>5
        ],
        [
            'name' => "Phong Điền",
            'slug' => "phong-dien",
            'province_id'=>5
        ],
        [
            'name' => "Phú lộc",
            'slug' => "phu-loc",
            'province_id'=>5
        ],
        [
            'name' => "Phú vang",
            'slug' => "phu-vang",
            'province_id'=>5
        ],
        [
            'name' => "Quảng Điền",
            'slug' => "quang-dien",
            'province_id'=>5
        ]
    );
    }
}
