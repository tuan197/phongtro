<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('province')->insert([
            'name' => "Thừa Thiên Huế",
            'slug' => "hue"
        ],
        [
            'name' => "Đà Nẵng",
            'slug' => "da-nang"
        ],
        [
            'name' => "Hà Nội",
            'slug' => "ha-noi"
        ],
        [
            'name' => "TP Hồ Chí Minh",
            'slug' => "ho-chi-minh"
        ]
    );
    }
}
