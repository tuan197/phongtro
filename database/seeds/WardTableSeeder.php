<?php

use Illuminate\Database\Seeder;

class WardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ward')->insert([
            'name' => "An Cuu",
            'slug' => "an-cuu",
            'district_id'=>1
        ]);
    }
}
