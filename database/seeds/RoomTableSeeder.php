<?php

use Illuminate\Database\Seeder;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $toTruncate = ['room'];
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        App\Room::truncate();

        // default users
        for ($i=1; $i <= 20; $i++) { 
            DB::table('room')->insert([
                [
                    'title' => Str::random(10)." số ".$i,
                    'description' => 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.',
                    'price' => 450000+$i,
                    'area' => 20+$i,
                    'count_view' => 0,
                    'utility' => '{"1":1,"2":2}',
                    'address' => '24/33 An Dương Vương',
                    'ward_id' => 1,
                    'image' =>  '{"1":"a.png","2":"p.png"}',
                    'user_id' => 1,
                    'start' => 3,
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        }

        

        $this->command->info('Room Created!');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
