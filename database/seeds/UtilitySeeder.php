<?php

use Illuminate\Database\Seeder;
use App\Utility;

class UtilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $utility = [
            ['name' => "Tu do",'slug' => "tu-do",'icon'=> "a"],
            ['name' => "Wifi",'slug' => "wifi",'icon'=> "a"],
            ['name' => "An Ninh",'slug' => "an-ninh",'icon'=> "a"],
        ];
        foreach($utility as $uti){
            Utility::create($uti);
        }
    }
}
