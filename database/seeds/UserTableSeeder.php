<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        App\User::truncate();
        $count = (int) $this->command->ask('How many users do you need ?', 10);
        $this->command->info("Creating {$count} users.");

        // default users

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'avatar' => Str::random(10),
            'password' => '123123',
            'email_verified_at' => now(),
            'phone' => '0337636242',
            'type' => '1',
            'ward_id' => 1,
            'gender' => 'Nam',
            'address' => '24/33 An Dương Vương'
            
        ]);

        factory(App\User::class, $count)->create();
        $this->command->info($count . ' Users Created!');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
