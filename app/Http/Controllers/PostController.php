<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests\UpdatePost;
use App\Services\PostServices;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    //
    public function __construct(PostServices $postServices) {
        $this->postServices    = $postServices;
    }
    public function getTempWrite()
    {
        return view('page.create-post');
    }
    public function createPost(PostRequest $request)
    {
       $post = $this->postServices->createPost(Auth::user()->id,$request);
       if($post){
           return redirect('post/get-post');
       }
    }
    public function getPostNew()
    {
        $arrPost = $this->postServices->getPost();
        //$arrPostPopulate = $this->postServices->getPostPopulate();
        return $arrPost;
        //return array_merge($arrPost,$arrPostPopulate);
        // return view('page.blog',compact('arrPost','arrPostPopulate'));
    }
    public function getPostTopView(){
        return $this->postServices->getPostPopulate();
    }
    public function getNewPost(){
        return $this->postServices->getTop5NewPost();
    }
    public function getDetailPost($id)
    {
        $post =  $this->postServices->getDetailPost($id);
        $arrPostPopulate = $this->postServices->getPostPopulate();
        return view('page.detail-post',compact('post','arrPostPopulate'));
    }
    public function getPostByAuthor()
    {
        $arrPost = $this->postServices->getPostByAuthor(Auth::user()->id);
        return view('page.list-post',compact('arrPost'));
    }
    public function deletePost($id)
    {
        $result = $this->postServices->deletePost($id);
        if($result){
            return redirect('post/manager-post');
        }
    }
    public function getEditPost($id)
    {
        $post = $this->postServices->getEditPost($id);
        if($post){
            return view('page.edit-post',compact('post'));
        }
    }
    public function updatePost(UpdatePost $request,$id)
    {
        $result = $this->postServices->updatePost($request,$id);
        if($result){
            return redirect('post/get-edit/'.$id)->with('success2','Cập nhật bài viết thành công');
        }
    }
    public function getAllPost()
    {
        $arrPost = $this->postServices->getAllPost();
        return view('admin.page.post.list',compact('arrPost'));
    }
    public function deletePostAdmin($id)
    {
        $result = $this->postServices->deletePost($id);
        if($result){
            return redirect('admin/post/list');
        }
    }
    public function getCreatePost()
    {
        return view('admin.page.post.create');
    }
    public function postCreate(PostRequest $request)
    {
        $result = $this->postServices->createPost(Auth::user()->id,$request);
        if($result)
        {
            return redirect('admin/post/list');
        }
    }
    public function getDetailPostAdmin($id)
    {
        $post =  $this->postServices->getDetailPost($id);
        return view('admin.page.post.detail',compact('post'));
    }
}
