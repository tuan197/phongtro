<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\RoomServices;
use App\Services\UtilityServices;
use App\Services\CommentServices;
use App\Services\PostServices;
use App\Http\Requests\RoomRequest;
use App\Http\Requests\CommentRequest;
use App\Room;
use App\Utility;

class RoomController extends Controller
{

    public function __construct(RoomServices $roomServices,
                        UtilityServices $utilityServices,
                        CommentServices $commentServices,
                        PostServices $postServices
                        ) {
        //$this->middleware('auth');
        $this->roomServices    = $roomServices;
        $this->utilityServices = $utilityServices;
        $this->commentServices = $commentServices;
        $this->postServices    = $postServices;
    }
    //
    public function index()
    {
        // $user = Auth::user();
        // if ($user->can('create', Room::class)) {
        //     echo 'Current logged in user is allowed to create new posts.';
        //   } else {
        //     echo 'Not Authorized';
        //   }
        $room         = $this->roomServices->index();
        $roomFiveStar = $this->roomServices->getRoomFiveStar();
        $arrPost      = $this->postServices->getTop5NewPost();
        return view('page.home',\compact(['room','roomFiveStar','arrPost']));
    }
    public function getRoomTop(){
        return $this->roomServices->getRoomFiveStar();
    }
    public function create(RoomRequest $request)
    {
        if($this->roomServices->create($request,Auth::user()->id)){
            $message = "Đăng phòng thành công, hệ thống sẽ kiểm tra và phê duyệt";
        }
        else{
            $message = "Đăng phòng thất bại, vui lòng thử lại";
        }
        return view("page.create-room",compact('message'));
    }
    public function getDetail($id)
    {
        $dateCurrent  = date("Y/m/d H:i:sa");
        $room = $this->roomServices->getDetail($id);
        $utility = $this->roomServices->getUtilityOfRoom($room->utility);
        $comment = $this->commentServices->getCommentOfRoom($id);
        foreach ($comment as $key => $value) {
            // Declare and define two dates 
            $date1 = strtotime($dateCurrent);  
            $date2 = strtotime($value->created_at);  
            

            $diff = abs($date2 - $date1);  

            $years = floor($diff / (365*60*60*24));   
            $months = floor(($diff - $years * 365*60*60*24) 
                                        / (30*60*60*24));  
            $days = floor(($diff - $years * 365*60*60*24 -  
                        $months*30*60*60*24)/ (60*60*24)); 
            $hours = floor(($diff - $years * 365*60*60*24  
                - $months*30*60*60*24 - $days*60*60*24) 
                                            / (60*60));   
            $minutes = floor(($diff - $years * 365*60*60*24  
                    - $months*30*60*60*24 - $days*60*60*24  
                                    - $hours*60*60)/ 60);   
            $seconds = floor(($diff - $years * 365*60*60*24  
                    - $months*30*60*60*24 - $days*60*60*24 
                            - $hours*60*60 - $minutes*60)); 
            $time = ""; 
            $time = $seconds . " giây trước";
            if($minutes != 0){
                $time = $minutes ." phút trước";
            }
            if($hours != 0){
                $time = $hours ." giờ trước";
            }
            if($days != 0){
                $time = $days . " ngày trước";
            }
            $value['date'] = $time;
        }

        $image = json_decode($room->image,true);
        $roomRelate = $this->roomServices->getRoomByWardId($room->id,$room->ward_id);
        $data =  array(
            'room'=> $room,
            'utility' => $utility,
            'image' => $image,
            'comment'=> $comment,
            'roomRelate' => $roomRelate
        );
        return $data;
        //return view('page.detail',\compact(['room', 'utility','image','comment','roomRelate']));
    }
    public function getDangPhong()
    {
        //$utility = $this->utilityServices->getAllUtility();
        return view('page.create-room');
    }
    public function postDangPhong(Request $request)
    {
        dd($request);
    }
    public function getTemFilter()
    {
        $room = $this->roomServices->index();
        return view('page.filter-room',\compact('room'));
    }
    public function postFilter(Request $request)
    {
        //return $request;
        return $room = $this->roomServices->filterRoom($request);
        //return view('page.filter-room',compact('room'));
    }

    

    public function searchRoom(Request $request)
    {
        $room = $this->roomServices->searchRoomLocal($request); 
        return view('page.filter-room',compact('room'));
    }
    /**
     * 
     */
    public function getAllRoom()
    {
        $room = $this->roomServices->index();
        //dd($room);
        return view('admin.page.room.list',compact('room'));
    } 
    
    public function getRoomAround()
    {
        $room = $this->roomServices->getAll();
        return view('page.map-room',compact('room'));
    }

    public function getTemManager()
    {
        $room = $this->roomServices->getRoomByUser(Auth::user()->id);
        return view('page.manager-room',compact('room'));
    }

    public function getRoomDetail($id)
    {
        $room = $this->roomServices->getRoomDetail($id);
        $utility = json_decode($room->utility);
        $ui = Utility::whereIn('id',$utility)->get();
        $image = json_decode($room->image,true);
        $arrComment = $this->roomServices->getCommentRoom($id);
        return view('admin.page.room.detail',compact(['room','ui','image','arrComment']));
    }
    public function deleteRoom(Request $request)
    {
        $this->roomServices->deleteRoom($request);
        return redirect('admin/room/list');
    }
    public function closeRoom(Request $request)
    {
        return $this->roomServices->closeRoom($request);
    }
    public function closeRoomAdmin(Request $request)
    {
        $result =  $this->roomServices->closeRoomAdmin($request);
        if($result){
            return redirect('admin/room/list');
        }
    }
    public function commentRoom($id,CommentRequest $request)
    {
        $result = $this->roomServices->saveComment(Auth::user()->id,$id,$request);
        if($result){
            return redirect()->route('getDetail', ['id' => $id]);
        }
        
    }
    public function unPublicRoom($id)
    {
        $result = $this->roomServices->unPublicRoom($id);
        if($result){
            return redirect('room/manager-room');
        }
    }
    public function publicRoom($id)
    {
        $result = $this->roomServices->publicRoom($id);
        if($result){
            return redirect('room/manager-room');
        }
    }
    public function adminConfirmRoom($id)
    {
        $result = $this->roomServices->publicRoom($id);
        if($result){
            return redirect('admin/room/new-room');
        }
    } 
    public function editRoom($id)
    {
        $room = $this->roomServices->getDetail($id);
        $utilityRoom = $this->roomServices->getUtilityOfRoom($room->utility);
        $image = json_decode($room->image,true);
        return view('page.edit-room',compact(['room','utilityRoom','image']));
    }
    public function postEditRoom($id, Request $request)
    {
        $result = $this->roomServices->editRoom($id,$request);
        if($result){
            return redirect('room/edit-room/'.$id)->with(['success'=>"Cập nhật thành công"]);
        }
    }
    public function deleteRoomByUser($id)
    {
        $result=$this->roomServices->deleteRoomByUser($id);
        if($result){
            return redirect('room/manager-room');
        }
    }
    public function getRoomByProvince($id)
    {
        $room = $this->roomServices->getRoomByProvince($id);
        return view('page.filter-room',compact('room'));
    }
    public function rePortRoom(Request $request)
    {
        $user_id = "";
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
        $result = $this->commentServices->reportRoom($request,$user_id);
        if($result){
            return redirect('room/detail/'.$request->id)->with(['success'=>"Cảm ơn bạn đã gửi phản hồi, chúng tôi sẽ ghi nhận và xử lý."]);
        }
    }
    public function getRoomNotPublic()
    {
        $room =  $this->roomServices->getRoomNotPublic();
        return view('admin.page.room.room',compact('room'));
    }
    public function getReport()
    {
        $reportArr = $this->roomServices->getReport();
        return view('admin.page.room.report-room',compact('reportArr'));
    }
    public function detailReport($id,$room_id)
    {
        $report = $this->roomServices->getDetailReport($id,$room_id);
        $data = $this->roomServices->dataAnalysisRoom($room_id);
        $comment = $this->roomServices->getCommentRoom($room_id);

        return view('admin.page.room.detail-report',compact('report','data','comment'));
    }
    public function getListRoomBlock()
    {
        $roomArr = $this->roomServices->getListRoomBlock();
        return view('admin.page.room.block-room',compact('roomArr'));
    }
    public function restoreRoom($id)
    {
        $this->roomServices->undoRoomBlock($id);
        return redirect('admin/room/list-block-room');
    }
    public function deleteImage($id,$path)
    {
        $result = $this->roomServices->deleteImage($id,$path);
        
        return $result;
    }
}
