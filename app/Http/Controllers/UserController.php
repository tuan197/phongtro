<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserServices;
use App\Services\PostServices;
use App\Http\Requests\UserRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPassRequest;
use App\Http\Requests\AddressRequest;
Use App\Jobs\ResetPassword;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userServices;
    protected $postServices;
    public function __construct(UserServices $userServices,PostServices $postServices) {
        $this->userServices = $userServices;
        $this->postServices = $postServices;
    }
    //
    public function register()
    {
        return view('layouts.register');
    }
    public function postRegister(UserRequest $request)
    {
        $user = $this->userServices->registerUser($request);
        if($user){
            $message = "Đăng ký thành công";    
        }else{
            $message = "Đăng ký thất bại";
        }
        return view('layouts.register',compact('message'));
    }
    public function getLogin()
    {
        $param ="";
        $id="";
        if(isset($_GET['param'])){
            $param = $_GET['param'];
            $id = $_GET['id'];
        }
        return view('layouts.login',compact(['param','id']));
    }
    public function postLogin(LoginRequest $loginRequest)
    {
        if($this->userServices->loginUser($loginRequest)){
            if(isset($loginRequest->param)){
                return redirect()->route('getDetail', ['id' => $loginRequest->id]);
            }
            return \redirect('/');
        }else{
            $message = "Thông tin đăng nhập không chính xác";
            return view('layouts.login',compact('message'));
        }
    }
    public function getDangTin()
    {
        
    }
    public function getUpdateAdress()
    {
        return view('page.update');
    }
    public function postUpdateInfo(AddressRequest $request)
    {
        if($this->userServices->updateAddress($request)){
            return view('page.create-room');
        }
        else{
            return redirect('user/updateAdress');
        }
    }
    public function getAllUser()
    {
        $arrUser  = $this->userServices->getAllUser();
        return view('admin.page.user.user-list',compact('arrUser'));
    }
    public function updateLevel(Request $request)
    {

        $this->userServices->updatePermision($request->id,$request->level);
        return redirect('admin/user/list');
    }
    public function deleteUser(Request $request)
    {
        $this->userServices->deleteUser($request->id);
        return redirect('admin/user/list');
    }
    public function getTemReset()
    {
        return view('layouts.reset');
    }
    public function resetPass(Request $request)
    {
        $user = $this->userServices->resetPass($request);
        $this->dispatch(new ResetPassword($user));
        if(empty($user)){
            return view('layouts.reset')->with('mess','Email vừa nhập không tồn tại');
        }
        else{
            return view('layouts.reset')->with('messs','Vui lòng kiểm tra  mail để đổi mật khẩu mới');
        }
    }
    public function checkTokenReset($email,$token)
    {
        $user = $this->userServices->checkTokenReset($email,$token);
        
        if(!empty($user->email))
        {
            return view('layouts.new-password',compact('user'));
        }
        else{
            return view('layout.404');
        }
    }
    public function updateNewPass(ResetPassRequest $request)
    {
        $result = $this->userServices->updateNewPass($request);
        if($result){
            return view('layouts.login');
        }else{
            return view('layout.404');
        }
    }
    public function updateInfo(Request $request)
    {
        $result = $this->userServices->updateInfo($request);
        if($result){
            return redirect('room/manager-room');
        }
    }
    public function getListMemberOfMotel()
    {
        $arrRoom = $this->userServices->getRoomEmty(Auth::user()->id);
        $listUser = $this->userServices->getListMember(Auth::user()->id);
        return view('page.list-room',compact('arrRoom','listUser'));
    }
    public function addMemberOfMotel(Request $request)
    {
        $result = $this->userServices->addMemberRoom($request,Auth::user()->id);
        if($result){
            return redirect('user/manager/list');
        }
        else{
            return redirect('user/manager/list');
        }
    }
    public function deleteMemberRoom($id){
        $result = $this->userServices->deleteMemberRoom($id);
        if($result){
            return redirect('user/manager/list');
        }
    }
    public function deleteMemberOfMotel()
    {
        
    }
    public function getInfoMember()
    {
        $arrPost = $this->postServices->getPostByAuthor(Auth::user()->id);
        return view('page.info-page-member',compact('arrPost'));
    }
    public function getUser($id)
    {
        return $this->userServices->getUser($id);
    }
    public function updateMemberOfMotel(Request $request)
    {
        $result = $this->userServices->updateMemberOfMotel($request);
        if($result){
            return redirect('user/manager/list')->with(['error'=>"Người thuê đã tồn tại"]);
        }
    }
    public  function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
