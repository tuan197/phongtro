<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RoomServices;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoomServices $roomServices)
    {
        $this->roomServices = $roomServices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $room = $this->roomServices->index();
        return $room;
        return view('page.home',\compact('room'));
    }
    public function get404()
    {
        return view('page.404');
    }
}
