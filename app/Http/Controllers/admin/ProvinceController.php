<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use App\Services\AppServices;

class ProvinceController extends Controller
{
    //
    public function __construct(AppServices $appServices)
    {
        $this->appServices = $appServices;
    }
    public function getProvince(){
        $district = $this->appServices->getAllDistrict();
        $ward = $this->appServices->getAllWard();
        return view('admin.page.province.manager',compact('district','ward')); 
    }
    public function addProvince(Request $request)
    {
        $result = $this->appServices->addProvince($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }
    public function updateProvince(Request $request)
    {
        $result = $this->appServices->updateProvince($request);
        $province = $this->appServices->getAllProvince();
        return redirect('admin/province/get-province');
    }
    public function deleteProvince(Request $request)
    {
        $result = $this->appServices->deleteProvince($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }

    public function addDistrict(Request $request)
    {
        $result = $this->appServices->addDistrict($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }
    public function updateDistrict(Request $request)
    {
        $result = $this->appServices->updateDistrict($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }
    public function deleteDistrict(Request $request)
    {
        $result = $this->appServices->deleteDistrict($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }

    public function addWard(Request $request)
    {
        $result = $this->appServices->addWard($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }

    public function updateWard(Request $request)
    {
        $result = $this->appServices->updateWard($request);

        if($result){
            return redirect('admin/province/get-province');
        }
    }
    public function deleteWard(Request $request)
    {
        $result = $this->appServices->deleteProvince($request);
        if($result){
            return redirect('admin/province/get-province');
        }
    }
}
