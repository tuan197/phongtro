<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserServices;
use App\Services\roomServices;
use App\Http\Requests\LoginRequest;

class AdminController extends Controller
{
    //
    protected $roomServices;
    public function __construct(UserServices $userServices,RoomServices $roomServices) {
        $this->userServices = $userServices;
        $this->roomServices = $roomServices;
    }

    public function getHomePage()
    {
        $roomFiveStar = $this->roomServices->getRoomFiveStar();
        return view('admin.page.home',compact('roomFiveStar'));
    }
    public function getLogin()
    {
        return view('admin.page.login');
    }
    public function postLogin(LoginRequest $request)
    {
        $result = $this->userServices->loginAdmin($request);
        if($result == true){
            return redirect('admin/home');
        }
        else{
            $message = "Tài khoản không có quyền truy cập";
            return view('admin.page.login',compact('message'));
        }
    }
}
