<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class CheckCreateRoomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->ward_id == null && Auth::user()->address == null){
                return redirect('user/updateAdress');
            }
            return $next($request);
        }
        else
            return redirect('user/login');
    }
}
