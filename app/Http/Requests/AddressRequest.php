<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'province' => 'required',
            'district' => 'required',
            'ward' => 'required',
            'numberaddress' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'province.required' => 'Vui lòng chọn tỉnh, thành phố',
            'district.required' => 'Vui lòng chọn quận, huyện',
            'ward.required' => 'Vui lòng chọn phường, xã',
            'numberaddress.required' => 'Vui lòng nhập tên đường và số nhà',
        ];
    }
}
