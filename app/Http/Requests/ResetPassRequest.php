<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'password' => 'required|min:6',
            're_password' => 'required|same:password'
        ];
    }
    public function messages()
    {
        return [
            'password.required' => 'Không được bỏ trống mật khẩu',
            'password.min' => 'Mật khẩu tối thiệu phải dài 6 ký tự',
            're_password.same' => 'Mật khẩu không khớp',
            're_password.required' => 'Không được bỏ trống mật khẩu'
        ];
    }
}
