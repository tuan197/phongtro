<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'txtaddress' => 'required',
            'area' => 'required|numeric',
            'price' => 'required | numeric',
            'province' => 'required',
            'district' => 'required',
            'ward' => 'required'

        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng điền tiều đề ',
            'txtaddress.required' => 'Vui lòng gõ địa chỉ, để xác định trển bản đồ',
            'area.required' => 'Vui lòng nhập diện tích phòng',
            'price.required' => 'Vui lòng nhập giá phòng',
            'province.required' => 'Vui lòng chọn tỉnh/thành phố',
            'district.required' => 'Vui lòng chọn quận/huyện',
            'ward.required' => 'Vui lòng chọn phường/xã',
        ];
    }
}
