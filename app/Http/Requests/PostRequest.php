<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => 'required|min:300',
            'thumb' => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhập tiêu đề bài viết!',
            'title.max' => 'Tiêu đề quá dài',
            'content.required' => 'Vui lòng nhập nội dung bài viết',
            'content.min' => 'Nội dung bài viết quá ngắn',
            'thumb.mimes' => 'Vui lòng tải lên file ảnh',
            'thumb.required' => 'Vui lòng nhập ảnh đại diện cho bài viết',
            'thumb.max' => 'Kích thước ảnh quá lớn'
        ];
    }
}
