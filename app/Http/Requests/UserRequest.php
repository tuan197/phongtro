<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|min:6',
            're_password' => 'required|same:password|min:6'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'name.required' => 'Không được bỏ trống tên',
            'password.required' => 'Không được bỏ trống mật khẩu',
            're_password.same' => 'Mật khẩu không khớp',
            're_password.required' => 'Không được bỏ trống mật khẩu'
        ];
    }
}
