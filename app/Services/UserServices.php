<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Room;
use App\Member;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class UserServices
{
    public function registerUser($request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['confirmed'] = 0;
        $input['ward_id'] = NUll;
        $input['type'] = 0;
        $input['avatar'] = "avatar.png";
        return User::create($input);
    }
    public function loginUser($request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return true;
        }
        return false;
    }
    public function loginAdmin($request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['type'] = 2;
        if (Auth::attempt($credentials) == true) {
            return true;
        }
        return false;
    }
    public function updateUser($request)
    {
        dd(Auth::user()->id);
    }
    public function updateAddress($request)
    {
        $input  = $request->all();
        $user = User::findOrFail(Auth::user()->id);
        $user->address = $input["numberaddress"];
        $user->ward_id = $input["ward"];
        return $user->save();
    }
   
    public function getAllUser()
    {
        return User::where('type','!=',1)->paginate(10);
    }
    
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        return $user->delete();
    }
    public function updatePermision($id,$level)
    {
        $user = User::findOrFail($id);
        $user->type = $level;
        return $user->save();
    }
    public function getCountUser()
    {
        return  User::count();
    }
    public function resetPass($request)
    {
        $user = User::where('email','=',$request->email)->first();
        return $user;
    }
    public function checkTokenReset($email,$token)
    {
        $whereData = [
            ['email', $email],
            ['remember_token', $token]
        ];
        
        return $users = User::where($whereData)->first(); 
    }
    public function updateNewPass($request)
    {
        $user = User::findOrFail($request->id);
        $user->remember_token = "";
        $user->password = bcrypt($request->password);
        return $user->save();
    }
    public function updateAvatarUser($file,$user_id)
    {
        $user = User::findOrFail($user_id);
        $image_path = "uploads/images/".$user->avatar;
        if(File::exists($image_path)) {
            return File::delete($image_path);
        }
        $image = Storage::disk('public')->put('/images', $file);
        $tmp = explode("/",$image);
        return $tmp[1];
    }
    public function updateInfo($request)
    {
        $path = $this->updateAvatarUser($request->file('avatar'),$request->id);
        $user = User::findOrFail($request->id);
        $user->name = $request->username;
        $user->phone = $request->phonenumber;
        $user->avatar =  $path;
        return $user->save();
    }
    public function getUser($id)
    {
        $user = User::with('ward.district.province2')->where('id', '=', $id)->first();
        return $user;
    }
    public function getRoomEmty($user_id)
    {
        $arrRoom = Room::where([
                                ['user_id', '=', $user_id],
                                ['public', '=', 1]
                                ])->get();
        return $arrRoom;
    }
    public function addMemberRoom($request,$auth)
    {
        $user = Member::where('user_id','=',$request->user_id)->first();
        if(is_null($user)){
            $input = $request->only('user_id', 'room_id');
            $input['confirm'] = 1;
            $input['authroom_id'] = $auth;
            return Member::create($input);
        }
        else{
            return false;
        }
    }
    public function getListMember($id)
    {

        $arrIdMember = Member::with('user.ward.district.province2')->where('authroom_id','=',$id)->get();
        //dd($arrIdMember);
        //$arrUser = User::with('ward.district.province2')->whereIn('id',$arrIdMember)->get();
        return $arrIdMember;
    }
    public function deleteMemberRoom($id)
    {
        $user = Member::where('user_id','=',$id)->first();
        return $user->delete();
    }
    public function updateMemberOfMotel($request){
        $user = Member::where([
            ['user_id', '=', $request->iduser],
            ['room_id', '=', $request->idroom]
            ])->first();
        $user->confirm = $request->type;
        return $user->save();
    }
    
}