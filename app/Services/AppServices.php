<?php

namespace App\Services;
use Auth;
use App\Province;
use App\District;
use App\Ward;



class AppServices
{
    public function getAllProvince()
    {
        $arr = Province::with('district.ward')->get();
        return $arr;
    }
    public function updateProvince($request)
    {
        $province = Province::findOrFail($request->id);
        $province->name = $request->name;
        $province->slug = str_slug($request->name);
        return $province->save();
    }
    public function deleteProvince($request)
    {
        $province = Province::findOrFail($request->id);
        return $province->delete();
    }

    public function getAllDistrict()
    {
        $arr = District::all();
        return $arr;
    }
    public function updateDistrict($request)
    {
        $district = District::findOrFail($request->id);
        $district->name = $request->name;
        $district->province_id = $request->province_id;
        $district->slug = str_slug($request->name);
        return $district->save();
    }
    public function deleteDistrict($request)
    {
        $district = District::findOrFail($request->id);
        return $district->delete();
    }

    public function getAllWard()
    {
        $arr = Ward::all();
        return $arr;
    }
    public function updateWard($request)
    {
        $ward = Ward::findOrFail($request->id);
        $ward->name = $request->name;
        $ward->district_id = $request->district_id;
        $ward->slug = str_slug($request->name);
        return $ward->save();
    }
    public function deleteWard($request)
    {
        $ward = Ward::findOrFail($request->id);
        return $ward->delete();
    }
    public function addProvince($request)
    {
        $province = new Province();
        $province->name = $request->name;
        $province->slug = str_slug($request->name);
        return $province->save();
    }
    public function addDistrict($request)
    {
        $district = new District();
        $district->name = $request->name;
        $district->slug = str_slug($request->name);
        $district->province_id = $request->province_id;
        return $district->save();
    }
    public function addWard($request)
    {
        $ward = new Ward();
        $ward->name = $request->name;
        $ward->slug = str_slug($request->name);
        $ward->district_id = $request->district_id;
        return $ward->save();
    }
}