<?php

namespace App\Services;
use Auth;
use App\Room;
use App\Ward;
use App\District;
use App\Province;
use App\Utility;
use App\Report;
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\File;

class RoomServices
{
    public function index()
    {
        // return Room::with(['ward.district','user'])->where('public','=',1)->orderBy('created_at','DESC')->paginate(5, ['*'], 'room');
        return Room::with(['ward.district','user'])->where('public','=',1)->orderBy('created_at','DESC')->paginate(5);
    }
    public function getAll()
    {
        return Room::with(['ward.district'])->get();
    }
    public function createName()
    {
        return time()*1000;
    }
    public function uploadImage($file)
    {
        $arr = array();
        foreach ($file as $key => $value) {  
            $name = Storage::disk('public')->put('/images', $value);
            $tmp = explode("/",$name);
            array_push($arr,$tmp[1]);
        }
        return $arr;
    }
    public function create($request,$user_id)
    {
        $input = $request->all();
        $arrImage = $this->uploadImage($input['upload_file']);
        $model = new Room();
        $model->title = $input['title'];
        $model->description = $input['description'];
        $model->price = $input['price'];
        $model->area = $input['area'];
        $model->count_view = 0;
        $model->utility = json_encode($input['utility']);
        $model->address = explode(",",$input['txtaddress'])[0];
        $model->ward_id = $input['ward'];
        $model->image = json_encode($arrImage,JSON_FORCE_OBJECT);
        $model->user_id = $user_id;
        $model->star = 0;
        $model->public = 0;
        $model->accept = $input['optio'] == "all" ? 2 : ($input['optio'] == "male" ? 1 : 0);
        $model->latlng = json_encode([
                            0 => (float)$input['txtlat'],
                            1 => (float)$input['txtlng']
                        ],JSON_FORCE_OBJECT);
        
        return $model->save();
    }
    public function getDetail($id)
    {
        $room = Room::with('user')->findOrFail($id);
        $room->count_view = $room->count_view+1;
        $room->save();
        return $room;
    }
    public function getRoomDetail($id)
    {
        $room = Room::with(['user','ward.district.province2'])->findOrFail($id);
        return $room;
    }
    public function getUtilityOfRoom($utility)
    {
        $arr = json_decode($utility,true);
        $arrayUtility = Utility::whereIn('id', $arr)->get();
        return $arrayUtility;
    }
    public function filterRoom($request)
    {
        $result = Room::filter()->with(['ward.district'])->get();
        return $result;
    }
    public function getAddress($id)
    {
        $address = "";
        $ward = Ward::findOrFail($id);
        $district = District::findOrFail($ward->district_id);
        $provice = Province::findOrFail($district->province_id);
        return $ward->name.",".$district->name.",".$provice->name;
    }
    public function searchRoom($key)
    {
        $clients = ClientBuilder::create()->build();
        
        $results = $clients->search([
            "index" => "room",
            "body" =>[
                "query"=>[
                   
                    "bool"=>[
                        "must"=> [
                            [
                                "match"=> [
                                    "title"=>  $key->key
                                ]  
                            ],
                            // [
                            //     "match"=> [
                            //         "description"=> $key->key
                            //     ]  
                            // ]
                        ]
                    ]
                      
                ]
            ]
        ]);
        $len = count($results['hits']['hits']);
        $room = array();
        if($len > 0){
            for($i=0;$i<$len;$i++){
                $results['hits']['hits'][$i]['_source']['add'] = $this->getAddress($results['hits']['hits'][$i]['_source']['ward_id']);
                array_push($room,$results['hits']['hits'][$i]['_source']);
            }
        }
        return $room;
    }
    public function searchRoomLocal($key)
    {
        $room = Room::where('title', 'like', '%' . $key->key . '%')->paginate(6);
        if(count($room) <= 0){
            $room = Room::where('address', 'like', '%' . $key->key . '%')->paginate(6);
        }
        return $room;
    }
    public function getRoomByUser($id)
    {
        return Room::with(['ward.district'])->where('user_id','=',$id)->orderBy('created_at')->paginate(10);
    }
    public function getCountRoom()
    {
        return Room::count();
    }
    public function deleteRoom($request)
    {
        $room = Room::findOrFail($request->id);
        return $room->delete();
    }
    public function deleteRoomByUser($id)
    {
        $room = Room::findOrFail($id);
        return $room->delete();
    }
    public function closeRoom($request)
    {
        $room = Room::findOrFail($request->id);
        $room->public = 0;
        return $room->save();
    }
    public function closeRoomAdmin($request)
    {
        $room = Room::findOrFail($request->id);
        $room->public = 2;
        return $room->save();
    }
    public function calculateStarRoom($room_id)
    {
        $starOne = Comment::where(['star'=>1,'room_id'=>$room_id])->count();
        $starTwo = Comment::where(['star'=>2,'room_id'=>$room_id])->count();
        $starThree = Comment::where(['star'=>3,'room_id'=>$room_id])->count();
        $starFour = Comment::where(['star'=>4,'room_id'=>$room_id])->count();
        $starFive= Comment::where(['star'=>5,'room_id'=>$room_id])->count();
        $star = Comment::where('room_id','=',$room_id)->count();

        $starOne = ($starOne/$star)*100;
        $starTwo = ($starTwo/$star)*100;
        $starThree = ($starThree/$star)*100;
        $starFour = ($starFour/$star)*100;
        $starFive = ($starFive/$star)*100;
        $array = [$starOne,$starTwo,$starThree,$starFour,$starFive];


        $maxs = array_keys($array, max($array));

        $room = Room::findOrFail($room_id);
        $room->star = $maxs[0]+1;
        return $room->save();
    }
    public function getRoomByWardId($room_id,$ward_id)
    {
        $whereData = [
            ['ward_id', $ward_id],
            ['id', '<>', $room_id]
        ];
        return Room::where($whereData)->take(5)->get();
    }
    public function saveComment($user_id,$id,$request)
    {
        $cmt = $request->all();
        if(!isset($cmt['star'])){
            $cmt['star'] = 1;
        }
        $cmt['room_id'] = $id;
        $cmt['user_id'] = $user_id;
        $newComment = Comment::create($cmt);
        $this->calculateStarRoom($id);
        return $newComment;
    }
    public function getRoomFiveStar()
    {
        //return Room::with(['ward.district','user'])->whereBetween('star', [4, 5])->paginate(5, ['*'], 'roomstar');
        return Room::with(['ward.district','user'])->whereBetween('star', [4, 5])->paginate(5);
    }
    public function unPublicRoom($id)
    {
        $room = Room::findOrFail($id);
        
        $room->public = false;
        return $room->save();
    }
    public function publicRoom($id)
    {
        $room = Room::findOrFail($id);
        $user = User::findOrFail($room->user_id);
        if($user->type != 1){
            $user->type = 1;
            $user->save();
        }
        $room->public = true;
        return $room->save();
    }
    public function editRoom($id,$request)
    {
        $room = Room::findOrFail($id);
        $input = $request->all();
        if(!empty($input['upload_file'])){
            $arrImage = $this->uploadImage($input['upload_file']);
            $newImage =json_decode($room->image,true);
            $len = count($arrImage);
            
            for($i=0;$i<$len;$i++){
                array_push($newImage,$arrImage[$i]);
            }
            $room->image = json_encode($newImage,JSON_FORCE_OBJECT);
        }
        
        $room->title = $input['title'];
        $room->description = $input['description'];
        $room->price = $input['price'];
        $room->area = $input['area'];
        $room->utility = json_encode($input['utility']);
        if(!empty($input['txtaddress'])){
            $room->address = explode(",",$input['txtaddress'])[0];
            $room->latlng = json_encode([
                0 => (float)$input['txtlat'],
                1 => (float)$input['txtlng']
            ],JSON_FORCE_OBJECT);
        }       
        $room->accept = $input['optio'] == "all" ? 2 : ($input['optio'] == "male" ? 1 : 0);
        return $room->save();
    }
   
    public function getRoomByProvince($province_id)
    {
        $provice = Province::find($province_id);
        $district = $provice->district;
        $newArr = array();
        foreach ($district as $key => $value) {
            $ta = District::find($value->id);
            $arrWard = $ta->ward;
            for($i=0;$i<count($arrWard);$i++)
            {
                array_push($newArr,$arrWard[$i]->id);
            }
        }
        $room = Room::whereIn('ward_id',$newArr)->paginate(10);
        return $room;
    }
    public function getCommentRoom($id)
    {
        $arrComment = Comment::with('user')->where('room_id','=',$id)->get();
        return $arrComment;
    }
    public function getRoomNotPublic()
    {
        $room = Room::with(['user','ward.district.province2'])->where("public","=",0)->paginate(10);
        return $room;
    }
    public function memberRegisterRoom()
    {
        
    }
    public function getReport()
    {
        $reportArr = Report::where('id','>',0)->paginate(10);
        return $reportArr;
    }
    public function getDetailReport($id,$room_id)
    {
        $report = Report::findOrFail($id);
        $report->status = 0;
        $report->save();
        return $report;
    }
    public function dataAnalysisRoom($room_id)
    {
        $array = array();
        $starOne = Comment::where(['star'=>1,'room_id'=>$room_id])->count();
        $starTwo = Comment::where(['star'=>2,'room_id'=>$room_id])->count();
        $starThree = Comment::where(['star'=>3,'room_id'=>$room_id])->count();
        $starFour = Comment::where(['star'=>4,'room_id'=>$room_id])->count();
        $starFive= Comment::where(['star'=>5,'room_id'=>$room_id])->count();

        $star = Comment::where('room_id','=',$room_id)->count();

        if($star != 0){
            $starOne = ($starOne/$star)*100;
            $starTwo = ($starTwo/$star)*100;
            $starThree = ($starThree/$star)*100;
            $starFour = ($starFour/$star)*100;
            $starFive = ($starFive/$star)*100;
            $array = [$starOne,$starTwo,$starThree,$starFour,$starFive];
        }
        
        return $array;
    }
    public function getListRoomBlock()
    {
        $roomArr = Room::where('public','=',2)->paginate(10);
        return $roomArr;
    }
    public function undoRoomBlock($id)
    {
        $room = Room::findOrFail($id);
        $room->public = 1;
        return $room->save();
    }
    public function getNumReportRoom()
    {
        return Report::where('status','=',1)->count();
    }
    public function getRoomNew()
    {
        return Room::where('public','=',0)->count();
    }
    public function updateImageRoom($id,$name)
    {
        $room = Room::findOrFail($id);
        $newImage =json_decode($room->image,true);
        $index = array_search($name,$newImage);
        unset($newImage[$index]);  
        $room->image = json_encode($newImage,JSON_FORCE_OBJECT);
        return $room->save();
    }
    public function deleteImage($id,$path)
    {
        $result = $this->updateImageRoom($id,$path);
        if($result){
            $image_path = "uploads/images/".$path;
            if(File::exists($image_path)) {
                File::delete($image_path);
                return "true";
            }   
            return "false";
        }
        
    }
}