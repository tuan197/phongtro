<?php

namespace App\Services;
use Auth;
use App\Post;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PostServices
{
    public function uploadThumb($file)
    {
        $image = Storage::disk('public')->put('/images', $file);
        $tmp = explode("/",$image);
        return $tmp[1];
    }
    public function createPost($user_id,$request)
    {
        $input = $request->only('title', 'content', 'thumb');
        $input['thumb'] = $this->uploadThumb($request->file('thumb'));
        $input['views'] = 0;
        $input['user_id'] = $user_id;
        $input['status'] = 1;
        $input['slug'] = $input['title'];
        return Post::create($input);
    }
    public function getPost()
    {
        $arrPost = Post::with('user')->orderBy('id', 'DESC')->paginate(9);
        return $arrPost;
    }
    public function getDetailPost($id)
    {
        $post = Post::with('user')->where('id','=',$id)->first();
        $post->views = $post->views + 1;
        $post->save();
        return $post;
    }
    public function getTop5NewPost()
    {
        $arrPost = Post::with('user')->orderBy('id', 'DESC')->take(5)->get();
        return $arrPost;
    }
    public function getPostPopulate()
    {
        $arrPost = Post::with('user')->orderBy('views', 'DESC')->take(5)->get();
        return $arrPost;
    }
    public function getPostByAuthor($user_id)
    {
        $arrPost = Post::with('user')->where("user_id","=",$user_id)->orderBy('views', 'DESC')->paginate(10);
        return $arrPost;
    }
    public function deletePost($id)
    {
        $post = Post::findOrFail($id);
        return $post->delete();
    }
    public function getEditPost($id)
    {
        $post = Post::findOrFail($id);
        return $post;
    }
    public function updatePost($request,$id)
    {
        $post = Post::findOrFail($id);
        $path = $post->thumb;
        if($request->file('thumb')){
            $image_path = "uploads/images/".$post->thumb;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }   
            $path = $this->uploadThumb($request->file('thumb'));
        }
        $post->title = $request->title;
        $post->content = $request->content;
        $post->thumb = $path;
        return $post->save();//as
    }
    public function countPost()
    {
        return Post::count();
    }
    public function getAllPost()
    {
        return Post::with('user')->where('id','>',-1)->paginate(10);
    }
}