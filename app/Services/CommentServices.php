<?php

namespace App\Services;
use Auth;
use App\Room;
use App\Comment;
usE App\Report;


class CommentServices
{
    public function getCommentOfRoom($id)
    {
        $arrCmt = Comment::with('user')->where('room_id','=',$id)->get();
        return $arrCmt;
    }
    public function getTop5Comment()
    {
        
    }
    public function reportRoom($request,$user_id)
    {
        $report = new Report();
        $report->room_id = $request->id;
        $report->content = $request->content;
        if(!empty($user_id)){
            $report->user_id = $user_id;
        }
        return $report->save();
    }
}