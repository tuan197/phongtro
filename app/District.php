<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    protected $table = 'district';

    public function province()
    {
        return $this->hasOne('App\Province');
    }
    public function ward()
    {
        return $this->hasMany('App\Ward');
    }
    public function province2()
    {
        return $this->belongsTo('App\Province', 'province_id', 'id');
    }
}
