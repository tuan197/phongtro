<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use \Crypt;

class ResetPassword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $encrypted = Crypt::encrypt('secret');
        $this->user->remember_token = $encrypted;
        $this->user->save();
        $data= array(
            'email'=> $this->user->email,
            'token'=> $encrypted,
        );
        Mail::send('layouts.reset-email', ['data' => $data], function($message){
            $message->from('vantuankrn197@gmail.com', 'admin phongtro.com');
            $message->to($this->user->email)->subject('Reset password');
        });
        
    }
}
