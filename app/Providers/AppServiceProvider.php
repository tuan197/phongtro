<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AppServices;
use Illuminate\Support\Facades\View;
use App\Services\UtilityServices;
use App\Services\RoomServices;
use App\Services\UserServices;
use App\Services\PostServices;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        if (app()->environment('remote')) {
            URL::forceSchema('https');
        }

        $app = new AppServices();
        $utility = new UtilityServices();
        $room = new RoomServices();
        $user = new UserServices();
        $post = new PostServices();
        if(count($app->getAllProvince()) > 0){
            View::share('province', $app->getAllProvince());
        }
        if(count($utility->getAllUtility()) > 0){
            View::composer(['page.create-room', 'page.filter-room','page.edit-room'], function ($view) use ($utility) {
                $view->with('utility', $utility->getAllUtility());
            });
        } 
        if($room->getCountRoom() > 0){
            View::composer(['admin.layout.header-cpu'], function ($view) use ($room,$user,$post) {
                $view->with(['numRoom'=>$room->getCountRoom(),'numUser'=>$user->getCountUser(),'numPost'=>$post->countPost()]);
            });
            View::composer(['admin.layout.navbar'], function ($view) use ($room,$user,$post) {
                $view->with(['numReport'=>$room->getNumReportRoom(),'numRoom'=>$room->getRoomNew()]);
            });
        }   
    }
}
