<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Elasticquent\ElasticquentTrait;

class Room extends Model
{
    //
    use SoftDeletes;
    // use ElasticquentTrait;

    protected $table = 'room';
    protected $primaryKey = 'id';

    public $fillable = [
        'title',
        'description',
        'price',
        'area',
        'count_view',
        'utility',
        'address',
        'ward_id',
        'image',
        'user_id',
        'star',
        'accept',
        'public'
    ];
    // protected $mappingProperties = array(
        
    //     'title' => [
    //         'type' => 'text',
    //         "analyzer" => "standard",
    //     ],
    //     'description' => [
    //         'type' => 'text',
    //         "analyzer" => "standard",
    //     ],
    //     'address' => [
    //         'type' => 'text',
    //         "analyzer" => "standard",
    //     ]
        
    // );
    public function ward()
    {
        return $this->belongsTo('App\Ward', 'ward_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function scopeFilter($q)
    {
        if(request('ward')){
            $q->where('ward_id','=',request('ward'));
        }
        if (request('r-utility')) {
            $q->whereJsonContains('utility',request('r-utility'));
        }
        if(request('r-star')){
            $q->where('star',request('r-star'));
        }
        if(request('r-price')){
            switch (request('r-price')) {
                case 2:
                    $q->where('price','<',500000);
                    break;
                case 3:
                    $q->whereBetween('price',[500000,1000000]);
                    break;
                case 4:
                    $q->whereBetween('price',[1000000,2000000]);
                    break;
                case 5:
                    $q->whereBetween('price',[2000000,3000000]);
                    break;
                case 6:
                    $q->where('price','>',3000000);
                    break;
                default:
                    $q->where('price','>',0);
                    break;
            }
        }
        if(request('r-area')){
            switch (request('r-area')) {
                case 1:
                    $q->where('area','<',20);
                    break;
                case 2:
                    $q->whereBetween('area',[20,30]);
                    break;
                case 3:
                    $q->whereBetween('area',[30,50]);
                    break;
                case 4:
                    $q->whereBetween('area',[50,60]);
                    break;
                case 5:
                    $q->where('area','>',60);
                    break;
                default:
                    $q->where('area','>',0);
                    break;
            }
        }
        return $q;
    }
}
