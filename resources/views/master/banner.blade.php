<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="banner-logo">
                    <a href="/"><img src="./images/logo3.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="banner-menu">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><a href="/room/map-room">Tìm trọ quanh đây</a></li>
                        <li><a href="{{route('get.post.new')}}">Blog</a></li>
                        @auth
                            <li>
                                Xin chào : {{Auth::user()->name}}
                            </li>
                            <li>
                                @if (Auth::user()->type == 1)
                                    <a href="/room/manager-room">Quản lý</a>
                                @endif
                                @if (Auth::user()->type == 0)
                                    <a href="/user/get-info">Trang cá nhân</a>
                                @endif
                            </li>
                        @else
                            <li><a href="{{ route('user.getLogin') }}">Đăng nhập</a></li>
                            <li><a href="{{ route('user.registerview') }}">Đăng ký</a></li>
                        @endauth
                    </ul>
                </div>
                <label class="nav-bars-btn" for="nav-mobile-input">
                    <i class="fas fa-bars"></i>
                </label>
 
                <input type="checkbox" name="" class="nav_input" id="nav-mobile-input" >
                <label class="nav__overlay" for="nav-mobile-input">

                </label>
                <div class="menu-mobie-app">
                    <label class="nav-btn-close" for="nav-mobile-input">
                        <i class="fas fa-times"></i>
                    </label>
                    <ul>
                        <li><a href="/"> <span><i class="fas fa-home"></i></span>Trang chủ</a></li>
                        <li><a href="/room/map-room"><span><i class="fas fa-map-signs"></i></span>Tìm trọ quanh đây</a></li>
                        <li><a href="{{route('get.post.new')}}"><span><i class="fas fa-blog"></i></span>Blog</a></li>
                        @auth
                            <li>
                                Xin chào : {{Auth::user()->name}}
                            </li>
                            <li>
                                @if (Auth::user()->type == 1)
                                    <a href="/room/manager-room">Quản lý</a>
                                @endif
                                @if (Auth::user()->type == 0)
                                    <a href="/user/get-info">Trang cá nhân</a>
                                @endif
                            </li>
                        @else
                            <li><a href="{{ route('user.getLogin') }}"> <span><i class="fas fa-sign-in-alt"></i></span>Đăng nhập</a></li>
                            <li><a href="{{ route('user.registerview') }}"><span><i class="fas fa-sign-in-alt"></i></span>Đăng ký</a></li>
                        @endauth
                    </ul>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="banner-search">
                    <p>Website tìm kiếm nhà trọ miễn phí cho sinh viên</p>
                    <div class="banner-search-form">
                        <form action="{{route('search.room')}}" method="GET">
                            <input type="text" placeholder="Nhập từ khóa tìm kiếm" name="key">
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu-mobile">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <a href="">
                    <img src="images/logo6.png" alt="">
                </a>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5 menu-mobile-form">
                <form action="{{route('search.room')}}" method="GET">
                    <input type="text" placeholder="Nhập từ khóa để tìm kiếm" name="key">
                    <button type="submit"><i class="fas fa-search"></i></button>
                </form>
            </div>
            <div class="col-md-4">
               
            </div>
        </div>
    </div>
</div>