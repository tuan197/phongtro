<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tìm kiếm phòng trọ</title>
    <base href="{{asset('/')}}" />
    {{-- <base href="https://chothuetro.herokuapp.com/"/> --}}
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="fontawesome/css/solid.css">
    <link rel="stylesheet" href="fontawesome/css/brands.css">
    <link rel="stylesheet" href="fontawesome/css/regular.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
    <script src="js/main.js"></script>
    <script src="js/particles.js"></script>
    <script src="js/pagination.js"></script>
    <script src="js/slide.js"></script>

    <script src="ckeditor/ckeditor/ckeditor.js"></script>
    <style>
        .slidecontainer {
            width: 100%;
        }

        .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 25px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
        }

        .slider:hover {
        opacity: 1;
        }

        .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
        }

        .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
        border-radius: 50px;
        }
    </style>
</head>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=1033798563398006&autoLogAppEvents=1"></script>
    @yield("header")
    <div id="wrapper">
        @yield('content')
    </div>
    @include('master.footer')

    @yield('script')

</body>
</html>