
@section('header')
<header>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-3 top-ref">
                    <a href="/room/dangphong">
                        <i class="fas fa-plus-circle"></i>
                        <span>Đăng phòng trọ</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('master.banner')
</header>
@endsection