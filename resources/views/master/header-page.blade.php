<header>
    <div class="top">
        <div class="container">
            <div class="row">
                
                <div class="col-md-3 col-sm-3 col-xs-3 top-ref">
                    <a href="/room/dangphong">
                        <i class="fas fa-plus-circle"></i>
                        <span>Đăng phòng trọ</span>
                    </a>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <div class="menu-top">
                        <ul>
                            <li><a href="/">Trang chủ</a></li>
                            <li><a href="room/map-room">Tìm trọ quanh đây</a></li>
                            <li><a href="{{route('get.post.new')}}">Blog</a></li>

                            @auth
                                <li>
                                    <p style="color:#fff;font-size:16px">Xin chào : {{Auth::user()->name}}</p> 
                                </li>
                                <li>
                                    @if (Auth::user()->type == 1)
                                        <a href="/room/manager-room">Quản lý</a>
                                    @endif
                                    @if (Auth::user()->type == 0)
                                        <a href="/user/get-info">Trang cá nhân</a>
                                    @endif
                                </li>
                            @else
                                <li><a href="{{ route('user.getLogin') }}">Đăng nhập</a></li>
                                <li><a href="{{ route('user.registerview') }}">Đăng ký</a></li>
                            @endauth
                        </ul>
                    </div>
                    <label class="nav-bars-btn" for="nav-mobile-input" style="padding-top:2px">
                        <i class="fas fa-bars"></i>
                    </label>
                    <input type="checkbox" name="" class="nav_input" id="nav-mobile-input" >
                    <label class="nav__overlay" for="nav-mobile-input">
    
                    </label>
                    <div class="menu-mobie-app">
                        <label class="nav-btn-close" for="nav-mobile-input" >
                            <i class="fas fa-times"></i>
                        </label>
                        <ul>
                            <li><a href="/"> <span><i class="fas fa-home"></i></span>Trang chủ</a></li>
                            <li><a href="/room/map-room"><span><i class="fas fa-map-signs"></i></span>Tìm trọ quanh đây</a></li>
                            <li><a href="{{route('get.post.new')}}"><span><i class="fas fa-blog"></i></span>Blog</a></li>
                            @auth
                                <li>
                                    Xin chào : {{Auth::user()->name}}
                                </li>
                                <li>
                                    @if (Auth::user()->type == 1)
                                        <a href="/room/manager-room">Quản lý</a>
                                    @endif
                                    @if (Auth::user()->type == 0)
                                        <a href="/user/get-info">Trang cá nhân</a>
                                    @endif
                                </li>
                            @else
                                <li><a href="{{ route('user.getLogin') }}"> <span><i class="fas fa-sign-in-alt"></i></span>Đăng nhập</a></li>
                                <li><a href="{{ route('user.registerview') }}"><span><i class="fas fa-sign-in-alt"></i></span>Đăng ký</a></li>
                            @endauth
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="">
                        <img src="../../images/logo6.png" alt="">
                    </a>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 menu-mobile-form">
                    <form action="{{route('search.room')}}" method="GET">
                        <input type="text" placeholder="Nhập từ khóa để tìm kiếm" name="key">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                
                <div class="col-md-4"></div>
            </div>
        </div>
   </div>
</header>