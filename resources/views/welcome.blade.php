<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <script src="../js/jquery-3.4.1.min.js"></script>
        <script src="../js/slide.js"></script>
    </head>
    <body>
        <div id="app">     
            <router-view></router-view>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>

    </body>
</html>