<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="{{url('public/')}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="fontawesome/css/solid.css">
    <link rel="stylesheet" href="fontawesome/css/brands.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="css/login.css">
    <script src="js/particles.min.js"></script>
    <title>Login to website</title>

</head>
<body>
    <div id="wrapper">
        <div id="logreg-forms">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @isset($message)
            <div class="alert alert-danger">
                {{$message}}
            </div>
            @endisset
            <form class="form-signin" method="POST" action="{{ route('reset') }}">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (isset($mess))
                  <div class="alert alert-danger">
                      <p>{{$mess}}</p>
                  </div>
                @endif
                @if (isset($messs))
                  <div class="alert alert-success">
                      <p>{{$messs}}</p>
                  </div>
                @endif
                @csrf
                <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Thông tin email đăng nhập</h1>
                
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required="" autofocus="">
                
                <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Gửi</button>
                
                <hr>
                <!-- <p>Don't have an account!</p>  -->
                <a href="{{ route('user.registerview') }}">
                    <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Đăng ký tài khoản</button>
                </a>
            </form>
           
        </div>
        <canvas class="background"></canvas>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="/script.js"></script>
    <script>
        
      window.onload = function() {
        Particles.init({
          selector: '.background',
          color: '#75A5B7',
          maxParticles: 130,
          connectParticles: true,
          responsive: [
            {
              breakpoint: 768,
              options: {
                maxParticles: 80
              }
            }, {
              breakpoint: 375,
              options: {
                maxParticles: 50
              }
            }
          ]
        });
      };
    
    </script>
</body>
</html>