@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection

@section('content')
<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-link">
                    <li><a href="{{ url('/') }}">Trang chủ</a></li>
                    <li><a href="">Phòng trọ</a></li>
                    <li>{{$room->title}}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="room-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="room-detail-df">
                    @if (\Session::has('success'))
                        <div class="alert alert-warning">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                    <div class="room-detail-df-title">
                        <div class="room-detail-df-title__name">
                            <h4>{{ $room->title}}</h4>
                        </div>
                        <div class="room-detail-df-title__view">
                            <p><strong>{{$room->count_view}}</strong> lượt xem , Ngày đăng : <strong>{{$room->created_at}}</strong></p>
                            <p style="text-align:right"><button id="direc" class="btn btn-primary">Xem đường đi</button></p>
                        </div>
                    </div>
                   
                    <div id="room-detail-df-map">
                        
                    </div>
                    <div id="room-detail-df-map-direc">

                    </div>
                    <div class="room-detail-df-info">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Thông tin phòng</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 ">
                                <p>Gía phòng</p>
                                <p>{{number_format($room->price)}} đồng</p>
                            </div>
                            <div class="col-md-3">
                                <p>Diện tích</p>
                                <p>{{$room->area}} mét vuông</p>
                            </div>
                            <div class="col-md-3">
                                <p>Đặt cọc</p>
                                <p>0 đồng</p>
                            </div>
                            <div class="col-md-3">
                                <p>Phòng trọ cho</p>
                                <p>{{$room->accept == 1 ? "Nam" : ($room->accept == 2 ? "Nữ" : "Nam và Nữ")}}</p>
                            </div>
                           
                           
                            <div class="col-md-3">
                                <p>Wifi</p>
                                <p>50,000 đồng</p>
                            </div>
                            <div class="col-md-3">
                                <p>Trạng thái</p>
                                <p>{{$room->public == 1 ? "Còn phòng" : ""}}</p>
                            </div>
                            <div class="col-md-12">
                                <p>Địa chỉ</p>
                                <p>{{$room->address.", ".$room->ward->name.", ".$room->ward->district->name.", ".$room->ward->district->province2->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="room-detail-df-utility">
                        <div class="row">
                            <div class="col-md-4">
                                <h4> Tiện ích</h4>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($utility as $item)
                                <div class="col-md-4">
                                    <p><i class="{{$item->icon}}"></i>{{$item->name}}</p>
                                </div>
                            @endforeach  
                        </div>
                    </div>
                    <div class="room-detail-df-description">
                        <div class="row">
                            <div class="col-md-4">
                                <h4> Mô tả thêm</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{$room->description}}
                            </div>
                        </div>
                    </div>
                    <div class="room-detail-df-image">
                        <div class="row">
                            <div class="col-md-4">
                                <h4> Hình ảnh phòng trọ</h4>
                            </div>
                        </div>
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach ($image  as $key => $value )
                                    
                                    <div class="item {{$key ==0 ? "active" : ""}}" data-thumb="{{$key}}">
                                        <img src="uploads/images/{{$value}}">
                                    </div>
                                @endforeach
                            </div>
                        </div> 
                        <div class="clearfix">
                        <div id="thumbcarousel" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">

                                <div class="item active">
                                    @foreach ($image as $key => $item)
                                        @if ($key < 4)
                                            <div data-target="#carousel" data-slide-to="{{$key}}" class="thumb"><img src="uploads/images/{{$item}}"></div>
                                        @endif
                                    @endforeach
                                </div><!-- /item -->
                                @if (count($image) > 4)
                                    <div class="item">
                                        @foreach ($image as $key => $item)
                                            @if ($key >= 4)
                                                <div data-target="#carousel" data-slide-to="{{$key}}" class="thumb"><img src="uploads/images/{{$item}}"></div>
                                            @endif
                                        @endforeach
                                    </div><!-- /item -->
                                @endif
                                
                            </div><!-- /carousel-inner -->
                            <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div> <!-- /thumbcarousel -->
                        </div><!-- /clearfix -->
                    </div>
                    <div class="room-detail-df-comment">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Bình luận bài đăng</h4>
                            </div>
                        </div>
                        <div class="row bootstrap snippets">
                            <div class="col-md-12  col-sm-12">
                                <div class="comment-wrapper">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            Thông tin bình luận
                                        </div>
                                        <div class="panel-body">
                                            @if (Auth::check())
                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                <form action="room/comment-room/{{$room->id}}" method="post">
                                                    @csrf
                                                    <div class="bown">
                                                        <label for="">Số sao đánh giá:</label>
                                                    <div class="rating">
                                                        <label>
                                                            <input type="radio" name="star" value="1" />
                                                            <span class="icon">★</span>
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="star" value="2" />
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="star" value="3" />
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>   
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="star" value="4" />
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="star" value="5" />
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                            <span class="icon">★</span>
                                                        </label>
                                                    </div>
                                                    </div>
                                                    
                                                    <textarea class="form-control" placeholder="write a comment..." rows="3" name="content"></textarea>
                                                    <br>
                                                    <input type="submit" value="Bình luận" class="btn btn-info pull-right">
                                                </form>
                                            @else
                                                <div class="alert alert-warning">
                                                    Vui lòng <a href="{{ route('user.getLogin') }}?param=detail&id={{$room->id}}"><strong>đăng nhập</strong></a> để thực hiện chức năng này
                                                </div>
                                                <p></p>
                                            @endif
                                            
                                            <div class="clearfix"></div>
                                            <hr>
                                            <ul class="media-list">
                                                @foreach ($comment as $item)
                                                    <li class="media">
                                                        <a href="#" class="pull-left">
                                                            <img src="https://bootdey.com/img/Content/user_1.jpg" alt="" class="img-circle">
                                                        </a>
                                                        <div class="media-body">
                                                            <span class="text-muted pull-right">
                                                                <small class="text-muted">{{$item['date']}}</small>
                                                            </span>
                                                            <strong class="text-success">{{$item->user->name}}</strong>
                                                            <span>
                                                                @if ($item->star > 0)
                                                                    Đánh giá
                                                                    @for ($i = 1; $i <= $item->star; $i++)
                                                                        <span class="fa fa-star checked"></span>
                                                                    @endfor
                                                                @endif
                                                               
                                                            </span>
                                                            <p>
                                                                {{$item->content}}
                                                            </p>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="room-detail-us">
                    <div class="room-detail-us-user">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="images/user.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <h5>Thông tin người đăng</h5>
                                <p>{{$room->user->name}}</p>
                                <p>Ngày tham gia : {{date('d-m-Y', strtotime($room->user->created_at))}}</p>
                            </div>
                            <div class="col-md-12">
                                <a class="btn btn-primary" href="callto:09123123123">{{$room->user->phone}}</a>
                            </div>
                            
                        </div>
                       
                    </div>
                </div>
                <div class="row bootstrap snippets room-detail-relate">
                    <div class="col-md-12  col-sm-12">
                        <div class="comment-wrapper">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Các phòng trọ trong khu vực
                                </div>
                               <div class="panel-body">
                                    @foreach ($roomRelate as $item)
                                        <a href="room/detail/{{$item->id}}">
                                            <div class="room-detail-relate-box">
                                                <div class="room-detail-relate-box-image">
                                                    <img src="uploads/images/{{json_decode($item->image,true)[0]}}" alt="">
                                                </div>
                                                <h5>{{$item->title}}</h5>
                                            </div>
                                        </a>
                                        
                                    @endforeach
                               </div>
                            </div>
                        </div>
                
                    </div>
                </div>
                <div class="row bootstrap snippets room-detail-report">
                    <div class="col-md-12  col-sm-12">
                        <div class="comment-wrapper">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Báo cáo bài đăng
                                </div>
                               <div class="panel-body">
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal" data-id="{{$room->id}}">Báo cáo bài đăng</button>
                               </div>
                            </div>
                        </div>
                
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thông tin báo cáo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="room/report-room" method="POST" id="target">
                @csrf
                <input type="hidden" class="form-control" id="recipient-name" name="id">
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Nội dung báo cáo:</label>
                    <textarea class="form-control" id="message-text" required="" name="content" oninvalid="this.setCustomValidity('Vui lòng nhập thông tin phản hồi')"
                    oninput="setCustomValidity('')"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Báo cáo</button>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>
<script src="js/report.js"></script>
<script>
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var recipient = button.data('id') 
        var token = $("input[name=_token]").val(); 
        var modal = $(this)

        modal.find('.modal-body input[name=id]').val(recipient)
    })


    $(':radio').change(function() {
        console.log('New star rating: ' + this.value);
    });
    <?php 
        echo "var array = ".$room.";";
        echo "var addr = '".$room->address.",".$room->ward->name.",".$room->ward->district->name."';";
    ?>
    
    var latlng = JSON.parse(array.latlng);
    var title = array.title;
    
    let startLatLng = [];
    let targetLatLng = [latlng[0], latlng[1]]; 
    let goalMarkerImg = 'https://test.l-svr.net/blog/images/marker-on-dummy.png';

    function initMap(){
        
        var options = {
            zoom : 16,
            center: {lat: latlng[0],lng: latlng[1]}
        }
        
        // New Map
        var map = new google.maps.Map(document.getElementById('room-detail-df-map'),options);
       
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                startLatLng[0] = parseFloat(pos.lat);
                startLatLng[1] = parseFloat(pos.lng);
                
            
            }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        }

        var marker = new google.maps.Marker({
            position : {lat: latlng[0],lng: latlng[1]},
            map: map,
            icon : 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
        });


        //
       



        var infoWindow = new google.maps.InfoWindow({
            content : `<div class="mapinfo" style="line-height:20px">
                            <div id="map-title" style="text-align:center;background:#003352;color:#fff;padding:6px;margin-bottom:5px">
                                ${array.title}
                            </div>
                            <div id="mapp-add">
                                ${addr}
                            </div>
                            <div id="map-phone">
                                Số điện thoại: ${array.user.phone}
                            </div>
                        </div>`
        });
        infoWindow.open(map, marker);
        marker.addListener('click',function(){
            infoWindow.open(map,marker);
        })
        $('#direc').click(function(){
            console.log(startLatLng);
            
            var mapp = document.getElementById('room-detail-df-map');
            var mapp2 = document.getElementById('room-detail-df-map-direc');
            if(mapp.style.display === "none"){
                mapp.style.display = "block";
                mapp2.style.display = "none";
            }else{
                mapp.style.display = "none";
                mapp2.style.display = "block";
            }
            
            function initialize() {
                let options = {
                    zoom: 16,
                    center: new google.maps.LatLng(startLatLng[0], startLatLng[1]),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('room-detail-df-map-direc'), options);
                let rendererOptions = {
                    map: map, 
                    draggable: true, 
                    preserveViewport: true 
                };
                let directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
                let directionsService = new google.maps.DirectionsService();
                directionsDisplay.setMap(map);

                console.log(startLatLng);
                
                let request = {
                    origin: new google.maps.LatLng(startLatLng[0], startLatLng[1]), 
                    destination: new google.maps.LatLng(targetLatLng[0], targetLatLng[1]), 
                    travelMode: google.maps.DirectionsTravelMode.WALKING, 
                };
                directionsService.route(request, function(response,status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                    new google.maps.DirectionsRenderer({
                        map: map,
                        directions: response,
                        suppressMarkers: true 
                    });
                    let leg = response.routes[0].legs[0];
                    makeMarker(leg.end_location, markers.goalMarker, map);
                    setTimeout(function() {
                        map.setZoom(16); 
                    });
                    }
                });
                }

                function makeMarker(position, icon, map) {
                let marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    icon: icon
                });
                }

                let markers = {
                goalMarker:  google.maps.MarkerImage(
                    goalMarkerImg, // 
                    new google.maps.Size(24, 33), 
                    new google.maps.Point(0, 0), 
                    new google.maps.Point(12, 33), 
                    new google.maps.Size(24, 33)) 
                };

                initialize()
            
        })
    }
  

    




</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzlVX517mZWArHv4Dt3_JVG0aPmbSE5mE&callback=initMap">
</script>

@endsection

