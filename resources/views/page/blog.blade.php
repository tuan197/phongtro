@extends('master.masters')

@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-link">
                        <li><a href="{{ url('/') }}">Trang chủ</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container ">

        <div class="row">
            <div class="col-md-8">
                <div class="blog">
                    <div class="title">
                        <h3>Bài viết trong blog</h3>
                    </div>
                    <div class="content">
                        <div class="row">
                                @foreach ($arrPost as $item)
                                    <a href="{{route('post.detail',['id'=>$item->id])}}">
                                        <div class="col-md-12">
                                            <div class="content__box-post">
                                                <div class="content__box-post__image-post">
                                                    <img src="uploads/images/{{$item->thumb}}" alt="">
                                                </div>
                                                <div class="content__box-post__title-post">
                                                    <h4>{{$item->title}}</h4>
                                                    
                                                        <?php 
                                                            echo \Illuminate\Support\Str::words(htmlspecialchars_decode($item->content), 30)
                                                            ?>
                                                   
                                                    <div class="content__box-post__info-post">
                                                        <p><i class="fas fa-user"></i>{{$item->user->name}}</p>
                                                        <p><i class="far fa-clock"></i>{{explode(' ',$item->created_at)[0]}}</p>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </a>
                                @endforeach                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="blog new-post">
                    <div class="new-post__title">
                        <h3>Bài viết được xem nhiều</h3>
                    </div>
                    <div class="new-post__box">
                        @foreach ($arrPostPopulate as $item)
                            <div class="new-post__box__mp">
                                <div class="new-post__box__mp__thumb">
                                    <img src="uploads/images/{{$item->thumb}}" alt="">
                                </div>
                                <div class="new-post__box__mp__title">
                                    <a href="{{route('post.detail',['id'=>$item->id])}}">
                                        <h5>{{$item->title}}</h5>
                                    </a>
                                </div>
                            </div>
                        @endforeach 
                    </div>
                </div>        
            </div>
        </div>
    </div>
   
@endsection
@section('script')

@endsection
