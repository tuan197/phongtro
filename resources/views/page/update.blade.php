@extends('master.masters')
@section('header')
    @include('master.header-page')    
@endsection
@section('content')
    <div class="update-form">
        @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form action="{{route('post.address')}}" method="post"> 
            @csrf
            <div class="form-group">
                <label for="" class="required">Chọn tỉnh, thành phố </label>
                <select name="province" id="province">
                    <option value="">Chọn tỉnh hoặc thành phố</option>
                    @foreach ($province as $item)
                        <option value="{{$item->id}}" class="prov">{{$item->name}}</option>
                    @endforeach
                </select>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label for="" class="required">Quận, huyện</label>
                <select name="district" id="district">
                    <option value="">Chọn quận, huyện, thị xã</option>
                    
                </select>
            </div>
            <div class="form-group">
                <label for="" class="required">Phường, xã, thị trấn</label>
                <select name="ward" id="ward">
                    <option value="">Chọn phường, xã, thị trấn</option>
                </select>
            </div>
            <div class="form-group">
                <label for="" class="required">Số nhà / Tên đường</label>
                <input type="text" name="numberaddress" id="">
            </div>
            <div class="form-group">
                <button type="submit" class="c-button c-button--large c-button--secondary">Cập nhật thông tin</button>
            </div>
        </form>
        
    </div>
@endsection
@section('script')
    <script>
        <?php 
            echo "var arr = ".$province .";";
        ?>
    </script>
    <script src="js/options.js"></script>
@endsection