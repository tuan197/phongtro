
@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="filter">
                    <div class="filter__title">
                        <h3>Bộ lọc</h3>
                    </div>
                    <div class="filter__options">
                        <form action="{{route('post.filter')}}" method="post" id="cpa-form">
                            @csrf
                            <div class="filter__options__address">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputPassword4">Tỉnh, thành phố</label>
                                        <select class="form-control" id="province" name="province">
                                            <option>Chọn tỉnh/thành phố</option>
                                            @foreach ($province as $item)
                                                <option value="{{$item->id}}" class="prov">{{$item->name}}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputPassword4">Quận, huyện</label>
                                        <select class="form-control" id="district" name="district">
                                            <option>Chọn quận/huyện</option>
                                          </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputPassword4">Phường, xã</label>
                                        <select class="form-control" id="ward" name="ward">
                                            <option>Chọn phường/xã</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="filter__options__price">
                                <h5>Giá phòng</h5>
                                <label class="container-fi">Tất cả
                                    <input type="radio"  name="r-price" value="1">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">Dưới 500K
                                    <input type="radio"  name="r-price" value="2">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">Từ 500 - 1 triệu
                                    <input type="radio"  name="r-price" value="3">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">1 triệu - 2 triệu
                                    <input type="radio"  name="r-price" value="4">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">2 triệu - 3 triệu
                                    <input type="radio"  name="r-price" value="5">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">Trên 3 triệu
                                    <input type="radio"  name="r-price" value="6">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="filter__options__area">
                                <h5>Diện tích</h5>
                                <label class="container-fi">Tất cả
                                    <input type="radio"  name="r-area" value="0">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">Dưới 20m2
                                    <input type="radio"  name="r-area" value="1">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">20m2 - 30m2
                                    <input type="radio"  name="r-area" value="2">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">30m2 - 50m2
                                    <input type="radio"  name="r-area" value="3">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">50m2 - 60m2
                                    <input type="radio"  name="r-area" value="4">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">Trên 60m2
                                    <input type="radio"  name="r-area">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="filter__options__utility">
                                <h5>Tiện ích</h5>
                                @foreach ($utility as $item)
                                    <label class="container2">{{$item->name}}
                                        <input type="checkbox" name="r-utility[]" value="{{$item->id}}">
                                        <span class="checkmark"></span>
                                    </label>
                                @endforeach
                            </div>
                            <div class="filter__options__start">
                                <h5>Đánh giá</h5>
                                <label class="container-fi">
                                    <span class="fa fa-star checked"></span>
                                    <input type="radio"  name="r-star" value="1">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <input type="radio"  name="r-star" value="2">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <input type="radio"  name="r-star" value="3">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <input type="radio"  name="r-star" value="4">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-fi">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <input type="radio"  name="r-star" value="5">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="button__submit">
                                <button type="submit" class="btn btn-primary">Áp dụng</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="room-new">
                    <div class="room-new-title">
                        <h3>Danh sách phòng trọ</h3>
                    </div>
                    <div class="data-container"></div>
                    <div id="room-new-first">
                        @foreach ($room as $value)
                        <?php 
                            $thumb = json_decode($value->image, true);
                            
                        ?>

                        <div class="room-new-content">
                            <a href="{{route('getDetail',['id' => $value->id])}}">
                                <div class="thumb">
                                    <img src="uploads/images/{{$thumb[0]}}" alt="">
                                </div>
                                <div class="info">
                                    <h3>{{$value->title}}</h3>
                                    <div class="info-main">
                                        <div class="info-main-text">
                                            <p><i class="fas fa-home"></i><span>Phòng trọ</span></p>
                                            <p>
                                                <i class="fas fa-user-friends"></i><span>{{$value->accept}}</span>
                                                <i class="fas fa-ruler"></i><span>{{$value->area}} m2</span>
                                            </p>
                                            <p><i class="fas fa-map-marker-alt"></i><span>{{$value->address .",". $value->ward->name .", ".$value->ward->district->name}}</span></p>
                                        </div>
                                        <div class="info-main-price">
                                            <p>{{number_format($value->price)}} Đ</p>
                                            <p>Phòng/Tháng</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    </div>
                   <div>
                       {{$room->links()}}
                   </div>
                    
                    <div id="pagination-demo2">
                    
                    </div>
                  
                    
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')


    <script>
        <?php 
            echo "var arr = ".$province .";";
        ?>
    </script>
    <script src="js/options.js"></script>
    <script>

        $(document).ready( function() { // Wait until document is fully parsed
            $("#cpa-form").on('submit', function(e){
                e.preventDefault();
                var formData = $(this).serialize();
                // formData = formData.split('&');
                // console.log(formData);
                var utility = [];
                $.each($("input[name='r-utility[]']:checked"), function(){
                    utility.push($(this).val());
                });
                var price = $('input[name="r-price"]:checked').val();
                var area = $('input[name="r-area"]:checked').val();
                var star = $('input[name="r-star"]:checked').val();
                var ward = $( "#ward option:selected" ).val();
                
                $.ajax({
                    type : 'post',
                    url: '/room/filter-room',
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "r-utility" : utility,
                        "r-price" : price,
                        "r-area" : area,
                        "r-star" : star,
                        "ward" : ward
                    },
                    success : function(data){
                        var tag = document.getElementById('room-new-first');

                        tag.innerHTML="";
                        
                        $(function() {   
                            (function(name) {
                            var container = $('#pagination-' + name);
                            container.pagination({
                                dataSource: data,
                                locator: 'items',
                                totalNumber: 50,
                                pageSize: 5,
                                showPageNumbers: true,
                                showPrevious: true,
                                showNext: true,
                                showNavigator: true,
                                showFirstOnEllipsisShow: true,
                                showLastOnEllipsisShow: true,
                                ajax: {
                                beforeSend: function() {
                                    container.prev().html('Loading data from  ...');
                                }
                                },
                                callback: function(response, pagination) {
                                window.console && console.log(22, response, pagination);
                                var dataHtml = '';

                                
                                $.each(response, function (index, item) {
                                    var obj = JSON.parse(item.image);                                   
                                    dataHtml += `
                                        <div class="room-new-content">
                                            <a href="room/detail/${item.id}">
                                                <div class="thumb">
                                                    <img src="uploads/images/${Object.entries(obj)[0][1]}" alt="">
                                                </div>
                                                <div class="info">
                                                    <h3>${item.title}</h3>
                                                    <div class="info-main">
                                                        <div class="info-main-text">
                                                            <p><i class="fas fa-home"></i><span>Phòng trọ</span></p>
                                                            <p>
                                                                <i class="fas fa-user-friends"></i><span>${item.accept == 0 ? "Nam/Nu" : (item.accept == 1 ? "Nam" : "Nu")}</span>
                                                                <i class="fas fa-ruler"></i><span> ${item.area}m2</span>
                                                            </p>
                                                            <p><i class="fas fa-map-marker-alt"></i><span>${item.address},${item.ward.name},${item.ward.district.name}</span></p>
                                                        </div>
                                                        <div class="info-main-price">
                                                            <p>${item.price.toLocaleString('vi', {style : 'currency', currency : 'VND'})}</p>
                                                            <p>Phòng/Tháng</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    `;
                                });

                                

                                container.prev().html(dataHtml);
                                }
                            })
                            })('demo2');
                        })
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                })
                
            });
        });
    </script>
@endsection
