@extends('master.masters')
@section('header')
@include('master.header-page')
@endsection
@section('content')
    <div class="manager-room">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row bootstrap snippets">
                        <div class="col-md-12  col-sm-12">
                            <div class="comment-wrapper">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Thông tin cá nhân
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="avatar-user">
                                                    <img src="/uploads/images/{{Auth::user()->avatar}}" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="user-info">
                                                            <label for="">Họ tên:</label>
                                                            <p>{{Auth::user()->name}}</p>
                                                        </div>
                                                        
                                                        <div class="user-info">
                                                            <label for="">Số điện thoại:</label>
                                                            <p>{{Auth::user()->phone}}</p>
                                                        </div>

                                                        <div class="user-info">
                                                            <label for="">Email:</label>
                                                            <p>{{Auth::user()->email}}</p>
                                                        </div>
                                                        <div class="user-info">
                                                            <label for="">Ngày đăng ký:</label>
                                                            <p>{{Auth::user()->created_at}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="options">
                                                            <div class="options__user">
                                                                @if (Auth::user()->ward_id == null)
                                                                    <a href="user/updateAdress">
                                                                        <button type="button" class="btn btn-primary"><i class="far fa-edit"></i> Cập nhật thông tin địa chỉ</button>
                                                                    </a>
                                                                @endif
                                                               
                                                                
                                                                <a href="{{route('post.getTempWrite')}}">
                                                                    <button type="button" class="btn btn-success"><i class="far fa-edit"></i> Viết bài</button>
                                                                </a>
                                                                <a href="user/logout">
                                                                    <button type="button" class="btn btn-dark">Đăng xuất</button>       
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row bootstrap snippets">
                        <div class="col-md-12  col-sm-12">
                            <div class="comment-wrapper">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Danh sách bài viết của bạn
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                           <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Tiêu đề</th>
                                                        <th scope="col">Ngày đăng</th>
                                                        <th scope="col">Lượt xem</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($arrPost as $item)
                                                        <tr>
                                                            <th scope="row">{{$item->id}}</th>
                                                            <td>{{$item->title}}</td>
                                                            <td>{{$item->created_at}}</td>
                                                            <td>{{$item->views}}</td>
                                                        </tr> 
                                                    @endforeach
                                                    
                                                    
                                                </tbody>
                                            </table>    
                                            </div>                     
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection

