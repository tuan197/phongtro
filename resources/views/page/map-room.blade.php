@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="container">
        <div class="row">
			<div class="col-md-12">
				<h3>Danh sách các phòng trọ xung quanh bạn.</h3>
			</div>
			<div class="col-md-12">
				<div data-role="main" class="ui-content">
					<form method="post" action="/action_page_post.php">
						<label for="">Kéo thanh dưới để thay đổi bán kính muốn tìm kiếm</label>
						<input type="range"  min="100" max="5000" value="1000" class="slider" id="myRange">
					</form>
					<p>Bán kính: <span id="demo"></span> (1km = 1000)</p>
				  </div>
			</div>
            <div class="col-md-12">
                <div id="map" style="height:500px"></div>
			</div>
        </div>
    </div>
@endsection
@section('script')
<script>
  
</script>
<script>

	var rad = function(x) {
		return x * Math.PI / 180;
	};

	var getDistance = function(p1, p2) {
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = rad(p2.lat() - p1.lat());
		var dLong = rad(p2.lng() - p1.lng());
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
			Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		return d; // returns the distance in meter
	};
  <?php 
    echo "var arrRoom = ".$room.";";
    echo "var array = ".$room.";";
  ?>
  	var arrListLatLng = [];
	var curLat,curLng;
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see the error "The Geolocation service
    // failed.", it means you probably did not give permission for the browser to
    // locate you.
    var map, infoWindow;
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 10
      });
      infoWindow = new google.maps.InfoWindow;
     
      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			curLat = pos.lat;
			curLng = pos.lng;
			checkLatLng(pos.lat,pos.lng,array,1000);
			

			//creat_bankinh(pos.lat,pos.lng,3000);
			infoWindow.setPosition(pos);
			infoWindow.setContent('Location found.');
			infoWindow.open(map);
			map.setCenter(pos);
          
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
      }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
      infoWindow.open(map);
    }
   
  function checkLatLng(lat,lng,arrRoom,radius)
  {
	
	var Vienna = new google.maps.LatLng( lat, lng ),
		map = new google.maps.Map( 
			document.getElementById( 'map' ),
			{
				zoom      : 14,
				center    : Vienna,
				mapTypeId : google.maps.MapTypeId.ROADMAP,

				panControl         : false,
				zoomControl        : false,
				scaleControl       : false,
				streetViewControl  : false,
				overviewMapControl : false
			}
		);


		var markerCenter = new google.maps.Marker( {
			map      : map,
			position : Vienna,
			draggable:true,
		} );
		map.setCenter(markerCenter.getPosition());
		//markerAPosition = new google.maps.LatLng( 48.23, 16.35 ),
		 //event drag marker (map);
		google.maps.event.addListener(markerCenter, 'dragend', function(evt) { 
			curLat = evt.latLng.lat().toFixed(6);
			curLng = evt.latLng.lng().toFixed(6);
			checkLatLng(evt.latLng.lat().toFixed(6),evt.latLng.lng().toFixed(6),array,1000);
		} );
		var rectangle = new google.maps.Rectangle({
			strokeColor   : '#FF0099',
			strokeOpacity : 0.8,
			strokeWeight  : 2,
			fillColor     : '#EE0990',
			fillOpacity   : 0.35,
			map           : map,
		} ),
		rectangleBounds = rectangle.getBounds(),
		circleRadius = radius, 
		circle = new google.maps.Circle( {
			map           : map,
			center        : Vienna,
			radius        : circleRadius,
			strokeColor   : '#FF0099',
			strokeOpacity : 1,
			strokeWeight  : 2,
			fillColor     : '#009ee0',
			fillOpacity   : 0.2
		} ),
		circleBounds = circle.getBounds();
		/**
		* Tests:
		* M*CB = Marker n Circle Bounds
		* M*RB = Marker n Rectange Bounds
		* M*SB = Marker n Spherical Bounds
		*/
		// Marker A


		arrRoom.forEach(element => {
			var ln = JSON.parse(element.latlng);
			
			markerPosition = new google.maps.LatLng( ln[0], ln[1] )
			var ma = new google.maps.LatLng( curLat, curLng );
			
			var MCB = circleBounds.contains( markerPosition );

			var ra = getDistance(markerPosition,ma);

			
			
			if(ra < radius){
				//var distanceBetween = Math.ceil(google.maps.geometry.spherical.computeDistanceBetween(markerCenter.getPosition(), markerPosition.getPosition()));
				markerB = new google.maps.Marker( {
					map      : map,
					position : markerPosition,
					icon: "images/gps.png",
				} );
				arrListLatLng.push(markerB);

				var content = contentTag(element.title,element.id);     

				var infowindow = new google.maps.InfoWindow()

				google.maps.event.addListener(markerB,'click', (function(markerB,content,infowindow){ 
					return function() {
					infowindow.setContent(content);
					infowindow.open(map,markerB);
					};
				})(markerB,content,infowindow)); 
			}
			
		});

	
	
  }
  function contentTag(title,id)
    {
        return `<div class="mapinfo" style="line-height:20px">
                            <div id="map-title" style="text-align:center;background:#003352;color:#fff;padding:6px;margin-bottom:5px">
                                ${title}
                            </div>
                            <div>
								<a href="https://phongtro.cfshusc.com/room/detail/${id}">Xem phòng</a>
							</div>
                            
                        </div>`;
    }

	var slider = document.getElementById("myRange");
	var output = document.getElementById("demo");
	output.innerHTML = slider.value;

	slider.oninput = function() {
		output.innerHTML = this.value;
		var rad = this.value;
		for(i=0; i<arrListLatLng.length; i++){
			arrListLatLng[i].setMap(null);
		}
		checkLatLng(curLat,curLng,array,parseInt(rad));
      
	}
  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzlVX517mZWArHv4Dt3_JVG0aPmbSE5mE&callback=initMap">
  </script>
@endsection