<!DOCTYPE >
<html>
   <head>
      
   </head>
   <body>
      <div id="mapholder"></div>
      <form>
         <input type="button" onclick="getLocation()" value="Get Location"/>
      </form>
      <h1>The onclick Event</h1>

<p>The onclick event is used to trigger a function when an element is clicked on.</p>

<p>Click the button to trigger a function that will output "Hello World" in a p element with id="demo".</p>

<button onclick="myFunction()">Click me</button>

<p id="demo"></p>
   </body>
   <script type="text/javascript">
    function myFunction() {
        document.getElementById("demo").innerHTML = "Hello World";
    }


    function showLocation(position) {
       var latitude = position.coords.latitude;
       var longitude = position.coords.longitude;
       var latlongvalue = position.coords.latitude + ","
       + position.coords.longitude;
       var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="
       +latlongvalue+"&amp;zoom=14&amp;size=400x300&amp;key=AIzaSyAa8HeLH2lQMbPeOiMlM9D1VxZ7pbGQq8o";
       document.getElementById("mapholder").innerHTML =
       "<img src='"+img_url+"'>";
    }
    function alerta(){
        console.log("asdsd");
        
    }
    function errorHandler(err) {
       if(err.code == 1) {
          alert("Error: Access is denied!");
       } else if( err.code == 2) {
          alert("Error: Position is unavailable!");
       }
    }
    function getLocation(){
       if(navigator.geolocation){
          // timeout at 60000 milliseconds (60 seconds)
          var options = {timeout:60000};
          navigator.geolocation.getCurrentPosition
          (showLocation, errorHandler, options);
       } else{
          alert("Sorry, browser does not support geolocation!");
       }
    }
 </script>
</html>