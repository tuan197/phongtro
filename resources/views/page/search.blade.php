@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-9">
                <div class="room-new">
                    <div class="room-new-title">
                        <h3>Danh sách phòng trọ</h3>
                    </div>
                    <div class="data-container"></div>
                    <div id="room-new-first">

                        @foreach ($room as $value)
                        <?php 
                            $thumb = json_decode($value['image'], true);
                            
                        ?>

                        <div class="room-new-content">
                            <a href="{{route('getDetail',['id' => $value['id']])}}">
                                <div class="thumb">
                                    <img src="images/{{$thumb[0]}}" alt="">
                                </div>
                                <div class="info">
                                    <h3>{{$value['title']}}</h3>
                                    <div class="info-main">
                                        <div class="info-main-text">
                                            <p><i class="fas fa-home"></i><span>Phòng trọ</span></p>
                                            <p>
                                                <i class="fas fa-user-friends"></i><span>{{$value['accept']}}</span>
                                                <i class="fas fa-ruler"></i><span>{{$value['area']}} m2</span>
                                            </p>
                                            <p><i class="fas fa-map-marker-alt"></i><span>{{$value['address'].",".$value['add']}}</span></p>
                                        </div>
                                        <div class="info-main-price">
                                            <p>{{number_format($value['price'])}} Đ</p>
                                            <p>Phòng/Tháng</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    </div>            
                </div>
            </div>
        </div>
    </div>
@endsection