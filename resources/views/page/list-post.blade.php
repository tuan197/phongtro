@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="manager-room">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row bootstrap snippets">
                        <div class="col-md-12  col-sm-12">
                            <div class="comment-wrapper">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Thông tin cá nhân
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="avatar-user">
                                                    <img src="/uploads/images/{{Auth::user()->avatar}}" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="user-info">
                                                            <label for="">Họ tên:</label>
                                                            <p>{{Auth::user()->name}}</p>
                                                        </div>
                                                        
                                                        <div class="user-info">
                                                            <label for="">Số điện thoại:</label>
                                                            <p>{{Auth::user()->phone}}</p>
                                                        </div>

                                                        <div class="user-info">
                                                            <label for="">Email:</label>
                                                            <p>{{Auth::user()->email}}</p>
                                                        </div>
                                                        <div class="user-info">
                                                            <label for="">Ngày đăng ký:</label>
                                                            <p>{{Auth::user()->created_at}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="options">
                                                            <div class="options__user">
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editInfo" data-id="{{Auth::user()->id}}">Cập nhật thông tin</button>
                                                                
                                                                <a href="{{route('post.getTempWrite')}}">
                                                                    <button type="button" class="btn btn-success"><i class="far fa-edit"></i> Viết bài</button>
                                                                </a>

                                                                <button type="button" class="btn btn-dark">Đăng xuất</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="nav__bar-manager">
                                <div class="list-group">
                                    <p class="list-group-item active">Menu</p>
                                    <a href="/room/manager-room" class="list-group-item">Danh sách phòng trọ</a>
                                    <a href="/user/manager/list" class="list-group-item">Thông tin sinh viên</a>
                                    <a href="/post/manager-post" class="list-group-item">Quản lý bài viết</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="heading">
                                <div class="title">
                                    <h3>Quản lý bài viết</h3>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Tiêu đề</th>
                                        <th scope="col">Nội dung</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Lượt xem</th>
                                        <th scope="col">Ngày đăng</th>
                                        <th scope="col">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($arrPost as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->title}}</td>
                                            <td><?php 
                                                echo \Illuminate\Support\Str::words(htmlspecialchars_decode($item->content), 30)
                                                ?></td>
                                            <td>{{$item->status}}</td>
                                            <td>{{$item->views}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>
                                                <a href="/post/get-edit/{{$item->id}}">
                                                    <button type="button" class="btn btn-info btn-xs"><i class="fas fa-pen"></i></button>
                                                </a>
                                               
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal" data-id={{$item->id}}>
                                                        <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                   
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
                
               
            </div>
        </div>
    </div>
     {{-- modal EDIT INFO --}}
    <div class="modal fade" id="editInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa thông tin cá nhân</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="user/update" onsubmit='return validate();' enctype="multipart/form-data">
                    <div  class="alert alert-warning" role="alert" id="error">

                    </div>
                    @csrf
                    <div class="form-group" id="userid">
                        <input type="hidden" name="id" id="iduser">
                    </div>
                    
                    <div class="form-group">
                        <label for="name" class="col-form-label">Họ tên:</label>
                        <input type="text" class="form-control" id="name" name="username" value="{{Auth::user()->name}}">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Số điện thoại:</label>
                        <input type="text" class="form-control" id="phone" name="phonenumber" value="{{Auth::user()->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label" style="display: block">Ảnh đại diện</label>
                        <img src="/uploads/images/{{Auth::user()->avatar}}" alt="" width="150px">
                        <input type="file" class="form-control" id="avatar" name="avatar">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
      </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa bài đăng này không ?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            </div>
            <div class="modal-footer">
                <div id="cf-delete">

                </div>
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
            </div>
        </div>
        </div>
    </div>
    <script>


        $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        console.log(recipient);
        var str = `
                <a href="post/delete/${recipient}" class="btn btn-danger">
                    Xóa
                </a>
                `;
        var ele = document.getElementById('cf-delete');
        ele.innerHTML = str;
        
        })
    $('.cancel_modal').click(function() {
    //alert('called');
        // we want to copy the 'id' from the button to the modal
        var href = $(this).data('target');
        var id = $(this).data('id');

        
        $('#confirm').html(`<a href="room/delete-room/${id}"><button type="button" class="btn btn-danger">Xóa</button></a>`);
    });

    $('#savebutton').click(function() {
        // now we grab it from the modal
        var id = $('#myModal').data('id');
        //var id=document.getElementById('myModal').getAttribute("data-id");
        alert(id);
    });

    $('#editInfo').on('show.bs.modal', function (event) {
        var ele = document.getElementById('error');
        ele.style.display = "none";
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body #userid input').val(recipient)
    })

    function validate(){
        var phone = document.getElementById("phone").value;
        var name = document.getElementById("name").value;
        if(phone === "" || name === ""){
            var ele = document.getElementById('error');
            
            var str = `
                <p>Vui lòng không bỏ trống họ tên hoặc số điện thoại</p>
            `;
            ele.innerHTML = str;
            ele.style.display = "block";
            return false;
        }
            
    }

  
  
</script>       
@endsection

