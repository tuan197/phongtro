@extends('master.masters')
@include('master.header-page')
@section('content')
<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-link">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="post/get-post">Blog</a></li>
                    <li>{{$post->title}}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <section class="content-post">
                <div class="content-post__title-post">
                    <h3>{{$post->title}}</h3>
                </div>
                <div class="content-post__content">
                    <?php 
                            echo htmlspecialchars_decode($post->content);
                        ?>
                    
                </div>
                <div class="content-post__info">
                    <p>Tác giả : <small><strong>{{$post->user->name}}</strong></small></p>
                </div>
                <div class="comment-fb">
                    <div class="fb-comments" data-href="http://phongtro.com/" data-numposts="5" data-width=""></div>
                </div>
            </section>
        </div>
        <div class="col-md-4">

            
            <div class="room-post">
                <div class="room-post-title">
                    <h3>Bài viết mới</h3>
                </div>
                @foreach ($arrPostPopulate as $item)
                    <a href="post/post-detail/{{$item->id}}">
                        <div class="room-post-content">
                            <div class="room-post-content__post-image">
                                <img src="uploads/images/{{$item->thumb}}" alt="">
                            </div>
                            <div class="room-post-content__post-title">
                                <h5>{{$item->title}}</h5>
                                <div class="room-post-content__post-title__info">
                                    <p><i class="fas fa-user"></i>TuanKrn197</p>
                                    
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach                         
            </div>
        </div>
    </div>
</div>


@endsection

