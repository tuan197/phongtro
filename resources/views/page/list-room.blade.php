@extends('master.masters')
@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="manager-room">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row bootstrap snippets">
                        <div class="col-md-12  col-sm-12">
                            <div class="comment-wrapper">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Thông tin cá nhân
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="avatar-user">
                                                    <img src="/uploads/images/{{Auth::user()->avatar}}" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="user-info">
                                                            <label for="">Họ tên:</label>
                                                            <p>{{Auth::user()->name}}</p>
                                                        </div>
                                                        
                                                        <div class="user-info">
                                                            <label for="">Số điện thoại:</label>
                                                            <p>{{Auth::user()->phone}}</p>
                                                        </div>

                                                        <div class="user-info">
                                                            <label for="">Email:</label>
                                                            <p>{{Auth::user()->email}}</p>
                                                        </div>
                                                        <div class="user-info">
                                                            <label for="">Ngày đăng ký:</label>
                                                            <p>{{Auth::user()->created_at}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="options">
                                                            <div class="options__user">
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editInfo" data-id="{{Auth::user()->id}}">Cập nhật thông tin</button>
                                                                
                                                                <a href="{{route('post.getTempWrite')}}">
                                                                    <button type="button" class="btn btn-success"><i class="far fa-edit"></i> Viết bài</button>
                                                                </a>

                                                                <button type="button" class="btn btn-dark">Đăng xuất</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="nav__bar-manager">
                                <div class="list-group">
                                    <p class="list-group-item active">Menu</p>
                                    <a href="/room/manager-room" class="list-group-item">Danh sách phòng trọ</a>
                                    <a href="/user/manager/list" class="list-group-item">Thông tin sinh viên</a>
                                    <a href="/post/manager-post" class="list-group-item">Quản lý bài viết</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            @if (\Session::has('error'))
                                <div class="alert alert-warning">
                                    <ul>
                                        <li>{!! \Session::get('error') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            <div class="heading">
                                <div class="title">
                                    <h3>Danh sách người thuê trọ</h3>
                                </div>
                                <div class="option">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addMember" data-whatever="@mdo">
                                        <i class="fas fa-plus"></i> Thêm
                                    </button>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Họ tên</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Số điện thoại</th>
                                        <th scope="col">Địa chỉ</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($listUser as $item)
                                        <tr>
                                            <td>{{$item->user->id}}</td>
                                            <td>{{$item->user->name}}</td>
                                            <td>{{$item->user->email}}</td>
                                            <td>{{$item->user->phone}}</td>
                                            <td data-toggle="tooltip" title="{{$item->user->ward->name}}, {{$item->user->ward->district->name}}, {{$item->user->ward->district->province2->name}}">{{$item->user->address}}</td>
                                            <td>
                                                @if ($item->confirm == 1)
                                                    <span class="label label-info">Đang thuê trọ</span>
                                                @else
                                                    <span class="label label-warning">Đã chuyển đi</span>
                                                @endif
                                            </td>
                                            <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editMember" data-id="{{$item->user->id}}" data-name="{{$item->user->name}}" data-types= "{{$item->confirm}}" data-room="{{$item->room_id}}"><i class="fas fa-pen"></i></button>
                                                
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" data-iduser={{$item->user->id}}>
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                             
                                            </td>
                                        </tr>
                                    @endforeach
                                   
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
                
               
            </div>
        </div>
    </div>
    {{-- modal edit type member --}}
    <div class="modal fade" id="editMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cập nhật thông tin người thuê</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="user/manager/updatetype" method="post">
            <div class="modal-body">
                @csrf
                <div class="form-group" id="userid">
                  <label for="idmember" class="col-form-label">Id:</label>
                  <input type="text" class="form-control" id="idmember" name="iduser" readonly>
                </div>
                <div class="form-group" id="roomid">
                    <label for="idroom" class="col-form-label">Phòng</label>
                    <input type="text" class="form-control" id="idroom" name="idroom" readonly>
                  </div>
                <div class="form-group" id="username">
                    <label for="username" class="col-form-label">Tên:</label>
                    <input type="text" class="form-control" id="username" required name="username">
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Thay đổi trạng thái</label>
                    <select class="form-control" id="typeMember" name="type">
                      <option value="1">Đang thuê trọ</option>
                      <option value="2">Đã chuyển đi</option>
                    </select>
                  </div>
             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Cập nhật</button>
            </div>
        </form>
          </div>
        </div>
      </div>
     {{-- modal EDIT INFO --}}
    <div class="modal fade" id="editInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa thông tin cá nhân</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="user/update" onsubmit='return validate();' enctype="multipart/form-data">
                    <div  class="alert alert-warning" role="alert" id="error">

                    </div>
                    @csrf
                    <div class="form-group" id="userid">
                        <input type="hidden" name="id" id="iduser">
                    </div>
                    
                    <div class="form-group">
                        <label for="name" class="col-form-label">Họ tên:</label>
                        <input type="text" class="form-control" id="name" name="username" value="{{Auth::user()->name}}">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Số điện thoại:</label>
                        <input type="text" class="form-control" id="phone" name="phonenumber" value="{{Auth::user()->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-form-label" style="display: block">Ảnh đại diện</label>
                        <img src="/uploads/images/{{Auth::user()->avatar}}" alt="" width="150px">
                        <input type="file" class="form-control" id="avatar" name="avatar">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
      </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa người thuê này không này không ?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-footer">
                <div id="delete-user">
                
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
            </div>
        </div>
        </div>
    </div>
  {{-- modal add member --}}
    <div class="modal fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm thành viên mới</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="user/manager/add" method="POST" onsubmit='return validate();'>
                        <div  class="alert alert-warning" role="alert" id="error2">

                        </div>
                        @csrf
                        <div class="form-group">
                            <label for="roomid" class="col-form-label">ID phòng:</label>
                            <select class="form-control" id="room" name="room_id">
                                @foreach ($arrRoom as $item)
                                    <option value="{{$item->id}}">{{$item->id}} - {{$item->title}}</option>
                                @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                            <label for="userid" class="col-form-label">ID thành viên:</label>
                            <input type="number" class="form-control" id="ckid" name="user_id">
                        </div>
                        <div id="info-user">
                            <p>Không có kết quả nào</p>
                        </div>
                        <button type="submit" class="btn btn-primary">Thêm</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#editMember').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') 
            var type = button.data('types')
            var room_id = button.data('room')
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            
            var select = document.getElementById('typeMember');
            for(var i=0;i<[...select].length;i++){
                console.log(select[i].value);
                if(select[i].value == type){
                    select.children[i].selected = 'selected';
                }
            }
            //

            
            modal.find('.modal-body #userid input').val(id)
            modal.find('.modal-body #username input').val(name)
            modal.find('.modal-body #roomid input').val(room_id)
        })

        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var idUser = button.data('iduser') 
            
            var modal = $(this)
            //modal.find('.modal-title').text('New message to ' + idUser)
            var ele = document.getElementById('delete-user');
            var str = `
                <a href="user/delete/${idUser}">
                    <button type="button" class="btn btn-warning">Xóa</button>
                </a>
            `;
            ele.innerHTML = str;
            
        })


    $('.cancel_modal').click(function() {
    //alert('called');
        // we want to copy the 'id' from the button to the modal
        var href = $(this).data('target');
        var id = $(this).data('id');

        
        $('#confirm').html(`<a href="room/delete-room/${id}"><button type="button" class="btn btn-danger">Xóa</button></a>`);
    });

    $('#savebutton').click(function() {
        // now we grab it from the modal
        var id = $('#myModal').data('id');
        //var id=document.getElementById('myModal').getAttribute("data-id");
        alert(id);
    });

    $('#editInfo').on('show.bs.modal', function (event) {
        var ele = document.getElementById('error');
        ele.style.display = "none";
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body #userid input').val(recipient)
    })

    function validate(){
        var phone = document.getElementById("phone").value;
        var name = document.getElementById("name").value;
        if(phone === "" || name === ""){
            var ele = document.getElementById('error');
            
            var str = `
                <p>Vui lòng không bỏ trống họ tên hoặc số điện thoại</p>
            `;
            ele.innerHTML = str;
            ele.style.display = "block";
            return false;
        }
            
    }


    // xử lý ajax gợi ý tên user
    $("#ckid").bind('keyup mouseup', function () {
        var id = this.value;
        var elementInfo = document.getElementById('info-user');
        
        $.ajax({
            type: 'GET',
            url : '/user/get-user/'+id,
            data : id,
            success :function(data){
                var str = "Không có kết quả nào cả";
                if(data !== ""){
                    var str = `
                        <p>ID : ${data.id}</p>
                        <p>Họ tên : ${data.name}</p>
                        <p>Họ tên : ${data.address}, ${data.ward.name}, ${data.ward.district.name}, ${data.ward.district.province2.name}</p>
                    `;
                }
                
                elementInfo.innerHTML = str;
                
            }
        })           
    });
    var ele2 = document.getElementById('error2');
    ele2.style.display = "none";
    
    function validate(){
        var id = document.getElementById("ckid").value;
        if(id === "" ){
            
            
            var str = `
                <p>Vui lòng nhập id người dùng</p>
            `;
            ele2.innerHTML = str;
            ele2.style.display = "block";
            return false;
        }
            
    }
</script>       
@endsection

