@include('master.header')
@extends('master.masters')

@section('content')
    <section class="trend">
        <div class="container">
            <div class="row">
                <div class="trend-title col-md-12">
                    <h2>Phòng trọ thuộc các khu vực</h2>
                </div>
                <div class="col-md-3">
                    <a href="room/get-room-by-province/1">    
                        <div class="trend-top">
                            <img src="images/trend.jpg" alt="">
                            <div class="trend-top-title">
                                <h3>Phòng trọ Huế</h3>
                            </div>
                            
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)" >    
                        <div class="trend-top">
                            <img src="images/trend.jpg" alt="">
                            <div class="trend-top-title">
                                <h3>Phòng trọ Đà Nẵng</h3>
                            </div>
                            
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)">    
                        <div class="trend-top">
                            <img src="images/trend.jpg" alt="">
                            <div class="trend-top-title">
                                <h3>Phòng trọ TP Hồ Chí Minh</h3>
                            </div>
                            
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)">    
                        <div class="trend-top">
                            <img src="images/trend.jpg" alt="">
                            <div class="trend-top-title">
                                <h3>Phòng trọ Hà Nội</h3>
                            </div>
                            
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="room">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="room-new">
                        <div class="room-new-title">
                            <h3>Phòng trọ mới</h3>
                            <a href="room/filter">Xem tất cả </a>
                        </div>
                        @foreach ($room as $value)
                        <?php 
                            $thumb = json_decode($value->image, true);
                            
                        ?>
                            <div class="room-new-content">
                                <a href="/room/detail/{{$value->id}}">
                                    <div class="thumb">
                                        <img src="uploads/images/{{$thumb[0]}}" alt="">
                                    </div>
                                    <div class="info">
                                        <h3>{{$value->title}}</h3>
                                        <div class="info-main">
                                            <div class="info-main-text">
                                                <p><i class="fas fa-home"></i><span>Phòng trọ</span></p>
                                                <p>
                                                    <i class="fas fa-user-friends"></i><span>{{$value->accept}}</span>
                                                    <i class="fas fa-ruler"></i><span>{{$value->area}} m2</span>
                                                </p>
                                                <p><i class="fas fa-map-marker-alt"></i><span>{{$value->address .",". $value->ward->name .", ".$value->ward->district->name}}</span></p>
                                            </div>
                                            <div class="info-main-price">
                                                <p>{{number_format($value->price)}} Đ</p>
                                                <p>Phòng/Tháng</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div>
                            {{$room->appends(['roomstar' => $roomFiveStar->currentPage()])->links()}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="room-post">
                        <div class="room-post-title">
                            <h3>Bài viết mới</h3>
                            <a href="{{route('get.post.new')}}">Xem tất cả</a>
                        </div>
                        @foreach ($arrPost as $item)
                            <a href="{{route('post.detail',['id'=>$item->id])}}">
                                <div class="room-post-content">
                                    <div class="room-post-content__post-image">
                                        <img src="uploads/images/{{$item->thumb}}" alt="">
                                    </div>
                                    <div class="room-post-content__post-title">
                                        <h5>{{$item->title}}</h5>
                                        <div class="room-post-content__post-title__info">
                                            <p><i class="fas fa-user"></i>{{$item->user->name}}</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </a>
                            
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="room">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="room-five_start">
                        <div class="room-new-title">
                            <h3>Phòng trọ được đánh giá tốt</h3>
                            <a href="room/filter">Xem tất cả</a>
                        </div>
                        @foreach ($roomFiveStar as $value)
                        <?php 
                            $thumb = json_decode($value->image, true);
                            
                        ?>
                            <div class="room-new-content">
                                <a href="{{route('getDetail',['id' => $value->id])}}">
                                    <div class="thumb">
                                        <img src="uploads/images/{{$thumb[0]}}" alt="">
                                    </div>
                                    <div class="info">
                                        <h3>{{$value->title}}</h3>
                                        <div class="info-main">
                                            <div class="info-main-text">
                                                <p><i class="fas fa-home"></i><span>Phòng trọ</span></p>
                                                <p>
                                                    <i class="fas fa-user-friends"></i><span>{{$value->accept}}</span>
                                                    <i class="fas fa-ruler"></i><span>{{$value->area}} m2</span>
                                                </p>
                                                <p><i class="fas fa-map-marker-alt"></i><span>{{$value->address .",". $value->ward->name .", ".$value->ward->district->name}}</span></p>
                                            </div>
                                            <div class="info-main-price">
                                                <p>{{number_format($value->price)}} Đ</p>
                                                <p>Phòng/Tháng</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div>
                            {{$roomFiveStar->appends(['room' => $room->currentPage()])->links()}}   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

