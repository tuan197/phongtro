@extends('master.masters')

@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="create-room">
                <div class="col-md-12">
                    <div class="create-room__title">
                        <h2>Đăng tin phòng trọ</h2>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="create-room__info-user">
                        <div class="create-room__info-user__avatar">
                            <img src="images/user.png" alt="">
                        </div>
                        <div class="create-room__info-user__info">
                            <h5>Thông tin người đăng</h5>
                            <p>Phạm Văn Tuấn</p>
                            <p>28/2/2020</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="create-room__lable">
                        <div class="create-room__lable__tag">
                            <h3>Thông tin phòng trọ</h3>
                        </div>
                        <div class="create-room__lable__form">
                            @isset($message)
                              <div class="alert alert-success">
                                {{$message}}
                              </div>
                            @endisset
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! \Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            
                            <form action="{{route('post.edit.room',["id"=>$room->id])}}" method="post" enctype="multipart/form-data">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                  <ul>
                                    @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                    @endforeach
                                  </ul>
                                </div>
                              	@endif
                                @csrf
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">Tiêu đề bài đăng</label>
                                    <input type="text" class="form-control" id="" name="title" value="{{$room->title}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Địa chỉ phòng trọ:</label>
                                    <input type="text" id="location-text-box" name="txtaddress" class="form-control" value="" />
                                    <input type="hidden" id="txtaddress" name="txtaddress" value=""  class="form-control"  />
                                    <input type="hidden" id="txtlat" value="" name="txtlat"  class="form-control"  />
                                    <input type="hidden" id="txtlng"  value="" name="txtlng" class="form-control" />
                                </div>
                                <div id="map-canvas" style="width: auto; height: 400px;" class="form-group col-md-12"></div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Diện tích (m2)</label>
                                        <input type="number" class="form-control" id="" name="area" value="{{$room->area}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Giá phòng (vnđ)</label>
                                        <input type="number" class="form-control" id="" name="price" value="{{$room->price}}">
                                    </div>
                                </div>
                                {{-- <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Tỉnh, thành phố</label>
                                        <select class="form-control" id="province" name="province">
                                            <option>Chọn tỉnh/thành phố</option>
                                            @foreach ($province as $item)
                                                <option value="{{$item->id}}" class="prov">{{$item->name}}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Quận, huyện</label>
                                        <select class="form-control" id="district" name="district">
                                            <option>Chọn quận/huyện</option>
                                          </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Phường, xã</label>
                                        <select class="form-control" id="ward" name="ward">
                                            <option>Chọn phường/xã</option>
                                          </select>
                                    </div>
                                </div> --}}
                            
                                <div class="form-group col-md-12">
                                    <label for="inputAddress">Tiện ích:</label>
                                    <div class="utility">
                                        @foreach ($utility as $item)
                                            @foreach ($utilityRoom as $items)
                                                <?php 
                                                    $item[$item->id] ="";
                                                    if($item->id === $items->id){
                                                        $item[$item->id] = "checked";
                                                        break;
                                                    }                                                
                                               ?>  
                                            @endforeach
                                            <label class="check">{{$item->name}}
                                                <input type="checkbox" value="{{$item->id}}" name="utility[]" {{$item[$item->id]}}>
                                                <span class="checkmark"></span>
                                            </label> 
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-12">
                                    <label for="exampleFormControlTextarea1">Loại phòng cho:</label>
                                  </div>	
                                  
                                  <div class="form-group col-md-4">
                                    <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="optio" id="male" value="1" {{$room->accept == 1 ? "checked" : ""}}>
                                        <label class="form-check-label" for="inlineRadio1">Nam</label>
                                    </div>
                                  </div>
                                  <div class="form-group col-md-4">
                                    <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="optio" id="female" value="2" {{$room->accept == 2 ? "checked" : ""}}> 
                                      <label class="form-check-label" for="inlineCheckbox1">Nữ</label>
                                    </div>
                                  </div>
                                  <div class="form-group col-md-4">
                                    <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="optio" id="all" value="0" {{$room->accept == 0 ? "checked" : ""}}>
                                      <label class="form-check-label" for="inlineCheckbox1">Nam và Nữ</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Mô tả ngắn</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description">{{$room->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Hình ảnh phòng</label>
                                    <div class="image-room">
                                        @foreach ($image as $item)
                                            <div class="image-room-ls">
												                        <span id="dele" onclick="dele(this,{{$room->id}})" data-path="{{$item}}">Xóa</span>
                                                <img src="uploads/images/{{$item}}" alt=""> 
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                </div>
                                <div class="form-group">

                                  <label for="comment">Thêm hình ảnh:</label>

                                  <div class="file-loading">
                                    <label for="upload_file" id="choose"><i class="fas fa-upload"></i> Chọn ảnh</label>
                                    <input type="file" hidden id="upload_file" name="upload_file[]" onchange="preview_image();" multiple/>
                                  </div>
                                  <div id="image_preview"></div>
                                </div>
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
		function dele(da,id)
		{     
			var path = da.getAttribute('data-path');
      
			$.ajax({
				type: 'GET',
				url : '/room/delete-image/'+id+"/"+path,
				data: path,
				success : function(data){
					if(data == "true"){
            da.parentNode.remove();
          }
          else{
            console.log("false");
            
          }
					
				}
			})
		}
		
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzlVX517mZWArHv4Dt3_JVG0aPmbSE5mE&callback=initialize&libraries=geometry,places" async defer></script>
    <script>
      var map;
      var marker;
      function initialize() {
        var mapOptions = {
          center: {lat: 16.070372, lng: 108.214388},
          zoom: 12
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);
    
      // Get GEOLOCATION
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude,
            position.coords.longitude);
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({
            'latLng': pos
          }, function (results, status) {
            if (status ==
              google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                
              } else {
                
              }
            } else {
              console.log('Geocoder failed due to: ' + status);
            }
          });
          map.setCenter(pos);
          marker = new google.maps.Marker({
            position: pos,
            map: map,
            draggable: true
          });
        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    
      function handleNoGeolocation(errorFlag) {
        if (errorFlag) {
          var content = 'Error: The Geolocation service failed.';
        } else {
          var content = 'Error: Your browser doesn\'t support geolocation.';
        }
    
        var options = {
          map: map,
          zoom: 19,
          position: new google.maps.LatLng(16.070372,108.214388),
          content: content
        };
    
        map.setCenter(options.position);
        marker = new google.maps.Marker({
          position: options.position,
          map: map,
          zoom: 19,
          icon: "images/gps.png",
          draggable: true
        });
        /* Dragend Marker */ 
        google.maps.event.addListener(marker, 'dragend', function() {
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                $('#location-text-box').val(results[0].formatted_address);
                $('#txtaddress').val(results[0].formatted_address);
                $('#txtlat').val(marker.getPosition().lat());
                $('#txtlng').val(marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
              }
            }
          });
        });
        /* End Dragend */
    
      }
    
      // get places auto-complete when user type in location-text-box
      var input = /** @type {HTMLInputElement} */
      (
        document.getElementById('location-text-box'));
    
    
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.bindTo('bounds', map);
    
      var infowindow = new google.maps.InfoWindow();
      marker = new google.maps.Marker({
        map: map,
        icon: "images/gps.png",
        anchorPoint: new google.maps.Point(0, -29),
        draggable: true
      });
    
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#txtaddress').val(results[0].formatted_address);
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            }
          }
        });
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          map.fitBounds(place.geometry.viewport);
        } else {
          map.setCenter(place.geometry.location);
          map.setZoom(17); // Why 17? Because it looks good.
        }
        marker.setIcon( /** @type {google.maps.Icon} */ ({
          url: "images/gps.png"
        }));
        document.getElementById('txtlat').value = place.geometry.location.lat();
        document.getElementById('txtlng').value = place.geometry.location.lng();
        
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
    
        var address = '';
        if (place.address_components) {
          address = [
          (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
        }
        /* Dragend Marker */ 
        google.maps.event.addListener(marker, 'dragend', function() {
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                $('#location-text-box').val(results[0].formatted_address);
                $('#txtlat').val(marker.getPosition().lat());
                $('#txtlng').val(marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
              }
            }
          });
        });
        /* End Dragend */
      });
    
    }
    
    
    // google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script>
        function preview_image() 
        {
            var total_file=document.getElementById("upload_file").files.length;
            for(var i=0;i<total_file;i++)
            {
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'><br>");
            }
        }
    </script>
@endsection
@section('script')
    <script>
        <?php 
            echo "var arr = ".$province .";";
        ?>
    </script>
    <script src="js/options.js"></script>
@endsection