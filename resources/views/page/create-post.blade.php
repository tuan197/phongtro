@extends('master.masters')

@section('header')
    @include('master.header-page')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3>Tạo bài viết mới </h3>
                </div>
                <div class="form-post">
                    <form action="post/create-post" method="post" enctype="multipart/form-data">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="text">Tiêu đề bài viết:</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Nhập tiêu đề bài viết">
                                </div>
                                <div class="form-group">
                                    <label for="text">Nội dung bài viết:</label>
                                    <textarea name="content" id="editor1" rows="10" cols="80" >
                                        
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="text">Ảnh đại diện:</label>
                                    <input type="file" name="thumb" id="" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Đăng bài viết</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection
@section('script')
    <script>
     
        CKEDITOR.replace( 'editor1',
		{
			filebrowserBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		});
    </script>
@endsection
