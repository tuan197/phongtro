@extends('admin.layout.layout')
@section('title')
    <title>Quản lý phòng trọ</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Danh sách bài viết</h1>
                </div><!-- /.col -->
                <div class="col-sm-6" style="text-align: right">
                    <a href="admin/post/create">
                        <button type="button" class="btn btn-primary" >Thêm mới bài viết</button>     
                    </a>  
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
       
        <table id="roomAll" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Tiêu đề</th>
                    <th>Ngày đăng</th>
                    <th>Chủ bài đăng</th>
                    <th style="text-align:center;width:100px;"> </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($arrPost as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>
                            <a href="admin/post/detail/{{$item->id}}">
                                {{$item->title}}
                            </a>
                        </td>
                        <td>{{$item->created_at}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>
                            
                            
                            <button type="button" class="btn btn-danger btn-xs dt-delete" data-toggle="modal" data-target="#exampleModal" data-id={{$item->id}}>
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $arrPost->links() }}
        </div>
    </section>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa bài đăng này không ?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            </div>
            <div class="modal-footer">
                <div id="cf-delete">

                </div>
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>   
            </div>
        </div>
        </div>
    </div>
@endsection
@section('script')


    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        console.log(recipient);
        var str = `
                <a href="admin/post/delete/${recipient}" class="btn btn-danger">
                    Xóa
                </a>
                `;
        var ele = document.getElementById('cf-delete');
        ele.innerHTML = str;
        
        })
    </script>
@endsection