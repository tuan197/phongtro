@extends('admin.layout.layout')
@section('title')
    <title>Quản lý phòng trọ</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Thêm mới bài viết</h1>
                </div><!-- /.col -->
                
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="form-post">
                <form action="admin/post/post-create" method="post" enctype="multipart/form-data">
                    @if ($errors->any())
                        <div class="alert alert-warning">
                            <ul style="list-style: none">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="text">Tiêu đề bài viết:</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Nhập tiêu đề bài viết">
                            </div>
                            <div class="form-group">
                                <label for="text">Nội dung bài viết:</label>
                                <textarea name="content" id="editor1" rows="10" cols="80" class="form-control">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="text">Ảnh đại diện:</label>
                                <input type="file" name="thumb" id="" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Đăng bài viết</button>
                    </div>
                </form>
            </div>  
        </div>
    </section>
   
@endsection
@section('script')
<script>
     
    CKEDITOR.replace( 'editor1',
    {
        filebrowserBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl : 'ckeditor/ckfinder/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : 'ckeditor/ckfinder/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
</script>
@endsection