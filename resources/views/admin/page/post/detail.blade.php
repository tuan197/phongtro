@extends('admin.layout.layout')
@section('title')
    <title>Quản lý bài viết</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Chi tiết bài viết</h1>
                </div><!-- /.col -->
           
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="detail__room">
                <div id="title">
                    <h5>Tiêu đề: {{$post->title}}</h5>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="content-post__content">
                                <?php 
                                        echo htmlspecialchars_decode($post->content);
                                    ?>
                                
                            </div>
                            <div class="content-post__info">
                                <p>Tác giả : <small><strong>{{$post->user->name}}</strong></small></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info-user">
                                <div id="title-user">
                                    Thông tin liên hệ chủ bài đăng
                                </div>
                                <div class="content-user">
                                    <img src="uploads/images/{{$post->user->avatar}}" alt="" style="width:100%">
                                    <p>Họ tên: <b>{{$post->user->name}}</b></p>
                                    <p>Số điện thoại: <b>{{$post->user->phone}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    
@endsection