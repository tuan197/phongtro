@extends('admin.layout.layout')

@section('contentadmin')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard </li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        @include('admin.layout.header-cpu')
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Danh sách phòng đánh giá tốt</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tên phòng</th>
                                    <th>Đánh giá</th>
                                    <th style="width: 40px">Chủ phòng</th>
                                </tr>
                                @foreach ($roomFiveStar as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>
                                            @for ($i = 1; $i <= 5; $i++)
                                                @if ($i<=$item->star)
                                                    <span class="fa fa-star checked"></span>                                  
                                                @else
                                                    <span class="fa fa-star"></span> 
                                                @endif
                                            @endfor
                                        </td>
                                        <td>
                                            {{$item->user->name}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->

    </div><!--/. container-fluid -->
</section>
@endsection