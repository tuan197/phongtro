@extends('admin.layout.layout')
@section('title')
    <title>Danh sách người dùng</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Quản lý dữ liệu tỉnh, thành</h1>
                </div><!-- /.col -->
           
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="province">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="list-group">
                        <p  class="list-group-item active">Tỉnh, thành phố 
                            <span data-toggle="modal" data-target="#addProvince" style="float: right"><i class="fas fa-plus"></i></span>
                        </p>
                        @foreach ($province as $item)
                        <div class="province__item list-group-item">
                            <p class="">
                                {{$item->name}}    
                            </p>
                            <div class="icon">
                                <button class="btn btn-primary" data-id="{{$item->id}}" data-whatever="{{$item->name}}" data-toggle="modal" data-type="province" data-target="#exampleModal">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button class="btn btn-danger" data-id="{{$item->id}}" data-whatever="{{$item->name}}" data-toggle="modal" data-type="province" data-target="#exampleModal2">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                                
                            </div>
                        </div>
                            
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="list-group">
                        <p class="list-group-item active">Quận, huyện <span data-toggle="modal" data-target="#addDistrict" style="float: right"><i class="fas fa-plus"></i></span></p>
                        @foreach ($district as $item)
                            <div class="province__item list-group-item">
                                <p class="">
                                    {{$item->name}}    
                                </p>
                                <div class="icon">
                                    <button class="btn btn-primary" data-id="{{$item->id}}" data-name="{{$item->name}}" data-provinceid="{{$item->province_id}}"  data-type="district" data-toggle="modal" data-target="#districtModal">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-danger" data-id="{{$item->id}}" data-whatever="{{$item->name}}"  data-type="district" data-toggle="modal" data-target="#exampleModal2">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                    
                                </div>
                            </div>
                            
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="list-group">
                        <p class="list-group-item active">Phường, xã <span data-toggle="modal" data-target="#addWard" style="float: right"><i class="fas fa-plus"></i></span></p>
                        @foreach ($ward as $item)
                            <div class="province__item list-group-item">
                                <p class="">
                                    {{$item->name}}    
                                </p>
                                <div class="icon">
                                    <button class="btn btn-primary" data-id="{{$item->id}}" data-name="{{$item->name}}" data-districtId="{{$item->district_id}}"  data-type="ward" data-toggle="modal" data-target="#wardModal">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-danger" data-id="{{$item->id}}" data-name="{{$item->name}}"  data-type="ward" data-toggle="modal" data-target="#exampleModal2">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                    
                                </div>
                            </div>
                            
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal edit-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cập nhật</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" >
              <form action="admin/province/update" method="POST" id="modal-edit">
                  @csrf
                 <input type="hidden" value="" id="id" name="id"> 
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label" >Tên:</label>
                  <input type="text" class="form-control" id="recipient-name" name="name">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>


     <!-- Modal delete-->
     <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Xác nhận xóa</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="admin/province/delete" method="POST" id="modal-delete">
                  @csrf
                 <input type="hidden" value="" id="idDelete" name="id"> 
                <div class="form-group" style="text-align:right">
                    <button type="submit" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </form>
            </div>
            
          </div>
        </div>
    </div>

    {{-- edit modal district --}}
    <div class="modal fade" id="districtModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cập nhật</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" >
              <form action="admin/district/update" method="POST" id="modal-edit-district">
                  @csrf
                 <input type="hidden"  id="idDistrict" name="id"> 
                <div class="form-group">
                  <label for="nameDistrict" class="col-form-label" >Tên Quận:</label>
                  <input type="text" class="form-control" id="nameDistrict" name="name">
                </div>
                <div class="form-group">
                    <label for="sel1">Select list:</label>
                    <select class="form-control" id="province_id" name="province_id">
                      @foreach ($province as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>


    {{-- edit modal wardModal --}}
    <div class="modal fade" id="wardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cập nhật</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <form action="admin/ward/update" method="POST" id="modal-edit-ward">
                    @csrf
                    <input type="hidden"  id="idWard" name="id"> 
                <div class="form-group">
                    <label for="nameWard" class="col-form-label" >Tên Quận:</label>
                    <input type="text" class="form-control" id="nameWard" name="name">
                </div>
                <div class="form-group">
                    <label for="sel1">Select list:</label>
                    <select class="form-control" id="district_id" name="district_id">
                        @foreach ($district as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addProvince" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Thêm tỉnh thành</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" >
              <form action="admin/province/add" method="POST">
                  @csrf
                 <input type="hidden" value="" id="id2" name="id"> 
                <div class="form-group">
                  <label for="recipient-name2" class="col-form-label" >Tên:</label>
                  <input type="text" class="form-control" id="recipient-name2" name="name">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>
    {{-- add district --}}

    {{-- add modal district --}}
    <div class="modal fade" id="addDistrict" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Thêm mới quận, huyện</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" >
              <form action="admin/district/add" method="POST" >
                  @csrf
                 <input type="hidden"  id="idDistrict2" name="id"> 
                <div class="form-group">
                  <label for="nameDistrict2" class="col-form-label" >Tên Quận, huyện:</label>
                  <input type="text" class="form-control" id="nameDistrict2" name="name">
                </div>
                <div class="form-group">
                    <label for="sel1">Select list:</label>
                    <select class="form-control" id="province_id2" name="province_id">
                      @foreach ($province as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thêm</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
    </div>

    {{-- add modal wardModal --}}
    <div class="modal fade" id="addWard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm phường, xã</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <form action="admin/ward/add" method="POST" >
                    @csrf
                    <input type="hidden"  id="idWard2" name="id"> 
                <div class="form-group">
                    <label for="nameWard2" class="col-form-label" >Tên phường, xã:</label>
                    <input type="text" class="form-control" id="nameWard2" name="name">
                </div>
                <div class="form-group">
                    <label for="sel1">Select list:</label>
                    <select class="form-control" id="district_id2" name="district_id">
                        @foreach ($district as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thêm</button>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var name = button.data('whatever') // Extract info from data-* attributes
            var id = button.data('id');
            var type = button.data('type');
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Cập nhật thông tin cho ' + name)
            modal.find('.modal-body input[type=text]').val(name)
            document.getElementById("id").value = id;
        })

        $('#districtModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var name = button.data('name') // Extract info from data-* attributes
            var id = button.data('id');
            var type = button.data('type');
            var provinceId = button.data('provinceid');
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Cập nhật thông tin cho ' + name)
            modal.find('.modal-body input[type=text]').val(name)
            document.getElementById("idDistrict").value = id;

            var selectProvince = document.getElementById('province_id');

            [...selectProvince.children].forEach(e=>{
                
                if(e.value === provinceId+''){
                    e.selected = true;              
                }
            })
            
        })
        $('#wardModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var name = button.data('name') // Extract info from data-* attributes
            var id = button.data('id');
            var idDistrict = button.data('districtid');
            var type = button.data('type');
            var selectDistrict = document.getElementById('district_id');

            [...selectDistrict.children].forEach(e=>{
                
                if(e.value === idDistrict+''){
                   e.selected = true;
                   
                }
            })
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Cập nhật thông tin cho ' + name)
            modal.find('.modal-body input[type=text]').val(name)
            document.getElementById("idWard").value = id;
            
        })

        $('#exampleModal2').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var name = button.data('whatever') // Extract info from data-* attributes
            var id = button.data('id');
            var type = button.data('type');
            if(type === "province"){
                document.getElementById('modal-delete').action = "admin/province/delete";
            }
            else if(type === "district"){
                document.getElementById('modal-delete').action = "admin/district/delete";
            }else if(type==="ward"){
                document.getElementById('modal-delete').action = "admin/ward/delete";   
            }
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Xác nhận xóa thông tin ' + name)
            modal.find('.modal-body input[type=text]').val(name)
            document.getElementById("idDelete").value = id;
        })
    </script>
@endsection