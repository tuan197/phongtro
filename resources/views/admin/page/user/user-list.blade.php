@extends('admin.layout.layout')
@section('title')
    <title>Danh sách người dùng</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Quản lý người dùng</h1>
                </div><!-- /.col -->
           
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
       
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Họ tên</th>
                    <th>Ngày đăng ký</th>
                    <th>Quyền</th>
                    <th>Trạng thái</th>
                    <th style="text-align:center;width:100px;"> </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($arrUser as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->created_at}}</td>
                        <td > 
                            @if ($item->type == "0")
                                <span class="label label-success">Thành viên</span>
                            @else
                                <span class="label label-primary">Chủ phòng</span>
                            @endif
                            
                        </td>
                        <td>
                            <span class="label label-info">Hoạt động</span>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs dt-edit" style="margin-right:16px;">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-xs dt-delete">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
        {{ $arrUser->links() }}
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                
                <div class="modal-body">
                    <form action="/admin/user/update" method="post">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="name">Họ tên:</label>
                            <input type="text" class="form-control" placeholder="Nhập họ và tên" id="name">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Chức vụ:</label>
                            <select class="form-control" id="level" name="level">
                                <option value="0">Thành viên</option>
                                <option value="1">Admin</option>
                                <option value="2">Chủ phòng</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>

            </div>
        </div>
        <div id="myModal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
               
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="model-header" id="m-header">
                        <h3>Xác nhận xóa user</h3>
                    </div>
                <div class="modal-body">
                    <form action="/admin/user/delete" method="post">
                        @csrf
                        <input type="hidden" name="id" id="idDelete">
                        <button type="submit" class="btn btn-danger">Xóa</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-footer">
                    
                </div>
                </div>

            </div>
        </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            //Only needed for the filename of export files.
            //Normally set in the title tag of your page.
            // DataTable initialisation
            $('#example').DataTable(
                {
                    "dom": '<"dt-buttons"Bf><"clear">lirtp',
                    "paging": false,
                    "autoWidth": true,
                    "columnDefs": [
                        { "orderable": false, "targets": 5 }
                    ],
                    "buttons": [
                        'colvis',
                        'copyHtml5',
                'csvHtml5',
                        'excelHtml5',
                'pdfHtml5',
                        'print'
                    ]
                }
            );
            //Add row button
            $('.dt-add').each(function () {
                $(this).on('click', function(evt){
                    //Create some data and insert it
                    var rowData = [];
                    var table = $('#example').DataTable();
                    //Store next row number in array
                    var info = table.page.info();
                    rowData.push(info.recordsTotal+1);
                    //Some description
                    rowData.push('New Order');
                    //Random date
                    var date1 = new Date(2016,01,01);
                    var date2 = new Date(2018,12,31);
                    var rndDate = new Date(+date1 + Math.random() * (date2 - date1));//.toLocaleDateString();
                    rowData.push(rndDate.getFullYear()+'/'+(rndDate.getMonth()+1)+'/'+rndDate.getDate());
                    //Status column
                    rowData.push('NEW');
                    //Amount column
                    rowData.push(Math.floor(Math.random() * 2000) + 1);
                    //Inserting the buttons ???
                    rowData.push('<button type="button" class="btn btn-primary btn-xs dt-edit" style="margin-right:16px;"><i class="fas fa-edit"></i></button><button type="button" class="btn btn-danger btn-xs dt-delete"><i class="fas fa-trash-alt"></i></button>');
                    //Looping over columns is possible
                    //var colCount = table.columns()[0].length;
                    //for(var i=0; i < colCount; i++){			}
                    
                    //INSERT THE ROW
                    table.row.add(rowData).draw( false );
                });
            });
            //Edit row buttons
            $('.dt-edit').each(function () {
                $(this).on('click', function(evt){
                    $this = $(this);
                    var dtRow = $this.parents('tr');
                    
                    
                    $('#id').val(dtRow[0].cells[0].innerHTML);
                    $('#name').val(dtRow[0].cells[1].innerHTML);
                    var select = $('#level');
                    var level = dtRow[0].cells[3].innerHTML;
                    
                    for(var i=0;i<select[0].children.length;i++){
                        if(select[0].children[i].innerHTML === level)
                            select[0].children[i].selected = "selected";
                    } 
                    $('#myModal').modal('show');
                });
            });
            //Delete buttons
            $('.dt-delete').each(function () {
                $(this).on('click', function(evt){
                    $this = $(this);
                    var dtRow = $this.parents('tr');

                    $('#idDelete').val(dtRow[0].cells[0].innerHTML);
                    $('#m-header').html(`<h5>Xác nhận xóa người dùng ${dtRow[0].cells[1].innerHTML}</h5>`);
                 
                    $('#myModal2').modal('show');
                });
            });
            // $('#myModal').on('hidden.bs.modal', function (evt) {
            //     $('.modal .modal-body').empty();
            // });
        });
    </script>
@endsection