@extends('admin.layout.layout')
@section('title')
    <title>Quản lý phòng trọ</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Danh sách phòng được đăng</h1>
                </div><!-- /.col -->
           
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
       
        <table id="roomAll" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Tiêu đề</th>
                    <th>Ngày đăng</th>
                    <th>Chủ bài đăng</th>
                    <th>Xếp hạng</th>
                    <th style="text-align:center;width:100px;"> </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roomArr as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>
                            <a href="admin/room/detail/{{$item->id}}">
                                {{$item->title}}
                            </a>
                        </td>
                        <td>{{$item->created_at}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>
                            @for ($i = 1; $i <= 5; $i++)
                                @if ($i<=$item->star)
                                    <span class="fa fa-star checked"></span>                                  
                                @else
                                    <span class="fa fa-star"></span> 
                                @endif
                            @endfor
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger btn-xs dt-delete">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                            <a href="admin/room/undoroom/{{$item->id}}">
                                <button type="button" class="btn btn-info btn-xs dt-delete">
                                    <i class="fas fa-undo"></i>
                                </button>
                            </a>
                            
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
        {{ $roomArr->links() }}
        <!-- Modal -->
        <div id="myModal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
               
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="model-header" id="m-header">
                        <h3>Xác nhận xóa user</h3>
                    </div>
                <div class="modal-body">
                    <form action="/admin/room/delete" method="post">
                        @csrf
                        <input type="hidden" name="id" id="idDelete">
                        <button type="submit" class="btn btn-danger">Xóa</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-footer">
                    
                </div>
                </div>

            </div>
        </div>
        </div>
    </section>
@endsection
@section('script')
    
@endsection