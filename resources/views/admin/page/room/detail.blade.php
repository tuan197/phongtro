@extends('admin.layout.layout')
@section('title')
    <title>Quản lý phòng trọ</title>
@endsection
@section('contentadmin')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Chi tiết phòng</h1>
                </div><!-- /.col -->
           
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="detail__room">
                <div id="title">
                    <h5>Tiêu đề: {{$room->title}}</h5>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <p>Chủ bài đăng: <span>{{$room->user->name}}</span></p>
                            <p>Địa chỉ : {{$room->address.", ".$room->ward->name.", ".$room->ward->district->name.", ".$room->ward->district->province2->name}}</p>
                            <p>Gía : {{number_format($room->price)}} đồng</p>
                            <p>Diện tích : {{$room->area}} m2</p>
                            <p>Ngày đăng : {{$room->created_at}}</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Các tiện ích:</p>
                                </div>
                                @foreach ($ui as $item)
                                    <div class="col-md-3">
                                        <span class="icon">
                                            <i class="{{$item->icon}}"></i>
                                        </span>
                                        {{$item->name}}
                                    </div>
                                @endforeach
                            </div>
                            <div class="image">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Hình ảnh:</p>
                                    </div>
                                    @foreach ($image as $item)
                                        <div class="col-md-4 image-room" >
                                            <img src="uploads/images/{{$item}}" alt="">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <p>
                                Mô tả : <br>
                                {{$room->description}}
                            </p>
                            <div class="button-confirm">
                                <p>
                                    <br>
                                    <a href="admin/room/block-room/{{$room->id}}">
                                        <button class="btn btn-warning" id="close-room">Đóng bài đăng</button>
                                    </a>
                                </p>
                                @if ($room->public == 0)
                                    <p>
                                        <br>
                                        <a href="admin/room/public-room/{{$room->id}}">
                                            <button class="btn btn-success" id="confirm-room">Duyệt bài đăng</button>
                                        </a>
                                    </p>
                                @endif
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="info-user">
                                <div id="title-user">
                                    Thông tin liên hệ chủ bài đăng
                                </div>
                                <div class="content-user">
                                    <img src="uploads/images/{{$room->user->avatar}}" alt="" style="width:100%">
                                    <p>Họ tên: <b>{{$room->user->name}}</b></p>
                                    <p>Số điện thoại: <b>{{$room->user->phone}}</b></p>
                                </div>
                            </div>
                            <div class="info-user">
                                <div id="title-user">
                                    Bình luận cho bài đăng
                                </div>
                                <div class="content-comment">
                                    <ul>
                                        @if (count($arrComment) > 0)
                                            @foreach ($arrComment as $item)
                                                <li><span>{{$item->user->name}}: </span>{{$item->content}}</li>
                                            @endforeach
                                        @else
                                            <li>Không có bình luận nào </li>
                                        @endif
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    
@endsection