<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="admin/home"" class="brand-link">
    <img src="assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
    <span class="brand-text font-weight-light">PhongTro</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        <a href="#" class="d-block">Phạm Văn Tuấn</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview menu-open">
            <a href="admin/home" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Bảng tin
                </p>
            </a> 
        </li>
      
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
                Phòng
                <i class="right fas fa-angle-left"></i>
            </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="admin/room/list" class="nav-link">
                    <i class="fas fa-chevron-right"></i>
                    <p>Danh sách tất cả phòng</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="admin/room/new-room" class="nav-link">
                    <i class="fas fa-chevron-right"></i>
                <p>Duyệt bài</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="admin/room/list-block-room" class="nav-link">
                    <i class="fas fa-chevron-right"></i>
                <p>Bài đăng đã khóa</p>
                </a>
            </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="fas fa-users"></i> 
            <p>
                 Quản lý người dùng
                <i class="fas fa-angle-left right"></i>
            </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="/admin/user/list" class="nav-link">
                        <i class="fas fa-chevron-right"></i>
                    <p>Danh sách người dùng</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-treeview">
            <a href="admin/room/get-report" class="nav-link">
                <i class="fas fa-users"></i> 
                 Quản lý phản hồi
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="admin/province/get-province" class="nav-link">
                <i class="fas fa-map-marker-alt"></i>
                <p>Quản lý tỉnh, thành</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="admin/post/list" class="nav-link">
                <i class="fas fa-newspaper"></i>
                <p>Quản lý bài viết</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="user/logout" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>
                <p>Đăng xuất</p>
            </a>
        </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>