/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("./bootstrap");
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faUserSecret,
    faHome,
    faUserFriends,
    faMapMarkedAlt,
    faRuler,
    faSearch,
    faUser,
    faFan,
    faWifi,
    faHSquare
} from "@fortawesome/free-solid-svg-icons";

import router from "./router/index.js";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import store from "./store/index.js";

import * as VueGoogleMaps from "vue2-google-maps";


window.Vue = require("vue");
Vue.use(ElementUI);
library.add([
    faUserSecret,
    faHome,
    faUserFriends,
    faMapMarkedAlt,
    faRuler,
    faSearch,
    faUser,
    faFan,
    faWifi,
    faHSquare
]);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyCzlVX517mZWArHv4Dt3_JVG0aPmbSE5mE",
        libraries: "places"
    }
});
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;
const app = new Vue({
    el: "#app",
    router,
    store
});
