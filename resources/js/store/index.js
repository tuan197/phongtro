import Vue from 'vue'
import Vuex from 'vuex'
import {fetchListRoomLastest,fetchListRoomTop} from '../api/room'
import {posts} from './post'
import {rooms} from './room'
import {loadingPage} from './loading'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    listRoomLastest:[],
    listRoomTop: [],
    loading:true
  },
  modules:{
    posts : posts,
    rooms : rooms,
    loadingPage
  },
  mutations: {
    fetchListLastest (state,list) {
      state.listRoomLastest = list;
    },
    fetchListTop(state,list){
      state.listRoomTop = list;
    },
    isLoading(state,value){
      state.loading = value;
    }
  },
  actions:{
    fetchList(context){
      fetchListRoomLastest().then(response => {
        if(response.data.length > 0){
            context.commit('fetchListLastest',response.data);
            context.commit('isLoading',false);
        }
    });
    },
    fetchListTop(context){
      fetchListRoomTop().then(response => {
        if(response.data.length > 0){
            context.commit('fetchListTop',response.data);
        }
      });
    }
  },
  
})
export default store;