import {getDetail} from '../api/room';

export const rooms = {
    state: () => ({ 
        room : {},
        comment : [],
        image : [],
        roomRelate: [],
        utility : [],
        author : {}
    }),
    mutations: {
        getDetail(state,room){
            state.room = room;
        },
        setAuthor(state,user){
            state.author = user
        },
        setUtility(state,arr){
            state.utility = arr
        },
        setImage(state,arr){
            state.image = arr
        }
    },
    actions: { 
        getDetail(context,id){
            getDetail(id).then(response => {
                if(response){          
                    context.commit('getDetail',response.room);
                    context.commit('setAuthor',response.room.user);
                    context.commit('setUtility',response.utility);
                    context.commit('setImage',response.image);
                    context.dispatch('hideLoading');
                }
            });  
        },
    },
    getters: { 

     }
  }