import {fetchListPostPolular,fetchListPostAll} from '../api/post';

export const posts = {
    state: () => ({ 
        listPostPopular : [],
        listPosts: []
    }),
    mutations: {
        fetchListPostPopular(state,list){
            state.listPostPopular = list;
        },
        fetchAllList(state,list){
            state.listPosts = [...list];
        }
    },
    actions: { 
        fetchListPostPopular(context){
            fetchListPostPolular().then(response => {
                if(response.length > 0){
                    context.commit('fetchListPostPopular',response);
                }
            });  
        },
        fetchAllList(context){
            fetchListPostAll().then(response => {
                if(response.data.length > 0){
                    context.commit('fetchAllList',response.data);
                }
            });  
        }
    },
    getters: { 

     }
  }