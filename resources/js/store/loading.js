export const loadingPage = {
    state: () => ({ 
        loading : true
    }),
    mutations: {
        setLoading(state,value){
            state.loading = value;
        }
    },
    actions: { 
        hideLoading(context){
            context.commit('setLoading',false)
        },
    }
}