import Vue from 'vue';
import VueRouter from 'vue-router';
import Router from 'vue-router';
import App from '../components/App.vue'
import Blog from '../components/Page/Blog.vue';
import FindRoomAround from '../components/Page/FindRoomAround'
import SearchRoomAround from '../components/Page/SearchRoomAround'
import MapG from '../components/Page/Map'
import RoomDetail from '../components/Page/RoomDetail';

Vue.use(Router);

const router  = new VueRouter({
    mode : 'history',
    routes: [
        { 
            path: '/', 
            name : 'app',
            component: App 
        },
        {
            path: '/blog',
            name: 'blog',
            component: Blog
        },
        {
            path: '/find-room-around',
            name: 'roomAround',
            component: FindRoomAround
        },
        {
            path: '/map',
            name: 'map',
            component: MapG
        },
        {
            path: '/room/:id',
            name: 'detail',
            component: RoomDetail
        },
    ]
})

export default router;