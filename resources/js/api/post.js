import request from '../utils/request';

export function fetchListPostPolular(query) {
  return request({
    url: '/api/post/get-new-post',
    method: 'get'
  });
}


export function fetchListPostAll(query) {
  return request({
    url: '/api/post/get-post',
    method: 'get'
  });
}

export function getListPostTopView(query){
  return request({
    url: '/api/post/get-post-top-view',
    method: 'get'
  });
}