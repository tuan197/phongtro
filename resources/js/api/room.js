import request from '../utils/request';

export function fetchListRoomLastest(query) {
  return request({
    url: '/api/room/get-room-lastest',
    method: 'get',
    params: query,
  });
}


export function fetchListRoomTop(query) {
  return request({
    url: '/api/room/get-room-top',
    method: 'get'
  });
}

export function getDetail(id) {
  return request({
    url: '/api/room/detail/'+id,
    method: 'get'
  });
}
