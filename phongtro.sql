-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 23, 2020 lúc 01:50 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `phongtro`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `room_id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `district`
--

CREATE TABLE `district` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `district`
--

INSERT INTO `district` (`id`, `name`, `slug`, `province_id`, `created_at`, `updated_at`) VALUES
(1, 'Thành phố Huế', 'thanh-pho-hue', 1, NULL, NULL),
(2, 'Thành phố Huế', 'thanh-pho-hue', 1, NULL, NULL),
(3, 'Thành phố Huế', 'thanh-pho-hue', 1, NULL, NULL),
(4, 'Thành phố Huế', 'thanh-pho-hue', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_09_090815_create_province_table', 2),
(4, '2020_02_09_084749_create_district_table', 3),
(5, '2020_02_09_090727_create_ward_table', 4),
(6, '2020_02_09_090908_create_uitility_table', 5),
(7, '2020_02_09_082138_create_room_table', 6),
(8, '2020_02_09_084451_create_comment_table', 7),
(9, '2020_02_09_091058_create_report_table', 7),
(10, '2020_02_13_015121_add_soft_deletes_to_room_table', 8),
(11, '2020_02_14_075106_add_accept_to_room_table', 9),
(12, '2020_02_14_081828_add_ward_id_to_users_table', 10),
(13, '2020_02_14_082717_add_gender_to_users_table', 11),
(14, '2020_02_18_075951_add_latlng_to_room_table', 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `province`
--

CREATE TABLE `province` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `province`
--

INSERT INTO `province` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Huế', 'hue', NULL, NULL),
(2, 'Huế', 'hue', NULL, NULL),
(3, 'Huế', 'hue', NULL, NULL),
(4, 'Huế', 'hue', NULL, NULL),
(5, 'Huế', 'hue', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `report`
--

CREATE TABLE `report` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `room_id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room`
--

CREATE TABLE `room` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `count_view` int(11) NOT NULL,
  `utility` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`utility`)),
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ward_id` bigint(20) UNSIGNED NOT NULL,
  `image` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`image`)),
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `start` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `accept` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latlng` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`latlng`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `room`
--

INSERT INTO `room` (`id`, `title`, `description`, `price`, `area`, `count_view`, `utility`, `address`, `ward_id`, `image`, `user_id`, `start`, `created_at`, `updated_at`, `deleted_at`, `accept`, `latlng`) VALUES
(1, 'ts9uCgfgVh số 1', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450001, 21, 10, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"anh1.png\",\"1\":\"anh2.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', '{\"0\":16.4488504,\"1\":107.6077789}'),
(2, 'LA7pZ1WAVI số 2', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450002, 22, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"anh3.png\",\"1\":\"anh4.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(3, 'ewriSWWfGH số 3', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450003, 23, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"anh5.png\",\"1\":\"anh6.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(4, 'kNRkFKFoie số 4', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450004, 24, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"anh7.png\",\"1\":\"anh8.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam', ''),
(5, 'W9Ay7NgNfF số 5', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450005, 25, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"anh9.png\",\"1\":\"anh10.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(6, 'OqrqdTMscv số 6', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450006, 26, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(7, 'NMOVPcobIS số 7', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450007, 27, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(8, '5Pw7SrxWy5 số 8', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450008, 28, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(9, 'CCb4i9TCm1 số 9', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450009, 29, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(10, 'sVCPdM9KDR số 10', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450010, 30, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(11, 'Ww1zcRnkFJ số 11', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450011, 31, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', ''),
(12, 'Btec7bg3SV số 12', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450012, 32, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam', ''),
(13, 'WW2XCdh9lo số 13', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450013, 33, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(14, 'l7KiWUZLIL số 14', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450014, 34, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam', ''),
(15, 'TtBeogr0QR số 15', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450015, 35, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(16, 'J2ZVx5Yj47 số 16', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450016, 36, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(17, 'thjmV5QxN1 số 17', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450017, 37, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(18, 'VUXtYEaTHG số 18', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450018, 38, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam', ''),
(19, '8KSRhDFkR0 số 19', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450019, 39, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nữ', ''),
(20, 'FeFSjoNePP số 20', 'Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium arcu ex. Aenean posuere libero eu augue condimentum rhoncus.', 450020, 40, 0, '{\"1\":1,\"2\":2}', '24/33 An Dương Vương', 1, '{\"0\":\"a.png\",\"1\":\"p.png\"}', 1, 3, '2020-02-09 07:41:07', '2020-02-09 07:41:07', NULL, 'Nam/Nữ', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ward_id` bigint(20) UNSIGNED DEFAULT NULL,
  `gender` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `avatar`, `password`, `email_verified_at`, `type`, `address`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `ward_id`, `gender`) VALUES
(1, 'waohJUPUPE', 'wW90QrpQ7r@gmail.com', '0337636242', '3nDEvQQJpJ', '123123', '2020-02-09 18:31:02', 1, '24/33 An Dương Vương', 0, NULL, '2020-02-09 18:31:02', NULL, 1, 0),
(2, 'Jeremy Glover', 'barrows.kole@example.org', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'HnnC6xu3Ty', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(3, 'Abigayle Graham', 'jana.goyette@example.net', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'hotk1dFItM', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(4, 'Cierra Beahan', 'mueller.ona@example.com', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, '6wsj6mlvV1', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(5, 'Candido Ritchie MD', 'bhilpert@example.com', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'u65Ggycvzd', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(6, 'Gage Robel II', 'eli.marks@example.org', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'FinSgRKv8L', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(7, 'Katarina Prosacco Sr.', 'vschulist@example.net', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'fGfEOXzdaH', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(8, 'Rafael Hodkiewicz', 'iondricka@example.org', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'BVNFTmHAWV', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(9, 'Orland Lueilwitz', 'edyth.tillman@example.com', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'd7rnfbTTEJ', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(10, 'Zena Parisian', 'salma.effertz@example.com', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'n9Z4SqfWSK', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(11, 'Devonte Kertzmann', 'amaya.rutherford@example.com', NULL, NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2020-02-09 18:31:02', 0, NULL, 0, 'mj7ve2sHkY', '2020-02-09 18:31:02', '2020-02-09 18:31:02', 1, 0),
(12, 'Phạm Văn Tuấn', 'admin@gmail.com', NULL, NULL, '$2y$10$uIaLSbXVVOEsuHLmuH1DWe5ORTk1qkJon8qc0PPNXfeTsea7GgdhG', NULL, 0, NULL, 0, NULL, '2020-02-16 09:19:24', '2020-02-16 09:19:24', NULL, NULL),
(13, 'Đi chơi', 'admin@admin.com', NULL, NULL, '$2y$10$x9qWY.8tet2isNuD/CsMzO0aqzC.evKvZxmCZZ6zuSbQHPyEhPPjq', NULL, 0, NULL, 0, NULL, '2020-02-16 09:20:15', '2020-02-16 09:20:15', NULL, NULL),
(14, 'Đi chơi', 'admin2@gmail.com', NULL, NULL, '$2y$10$REsDefG/vpngE95mtDMXoOOKneoQrQzxl8bHfzX2q7IOFvb7WIGYG', NULL, 0, NULL, 0, NULL, '2020-02-16 09:25:54', '2020-02-16 09:25:54', NULL, NULL),
(15, 'Tuan pham', 'tuan@gmail.com', NULL, NULL, '$2y$10$eqXKbNnzTUBGv6Amn6LzdOGLB7ngbfn3tfk.ZUKoNof825Hyunbl.', NULL, 0, NULL, 0, NULL, '2020-02-17 08:18:20', '2020-02-17 08:18:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `utility`
--

CREATE TABLE `utility` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `utility`
--

INSERT INTO `utility` (`id`, `name`, `slug`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'Chổ để xe', 'cho-de-xe', 'fas fa-fan', NULL, NULL),
(2, 'Wifi', 'wifi', 'fas fa-wifi', NULL, NULL),
(3, 'An ninh', 'an-ninh', '', NULL, NULL),
(4, 'Tự do', 'tu-do', 'fas fa-clock', NULL, NULL),
(5, 'Chủ riêng', 'chu-rieng', '', NULL, NULL),
(6, 'Gác lửng', 'gac-lung', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ward`
--

CREATE TABLE `ward` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ward`
--

INSERT INTO `ward` (`id`, `name`, `slug`, `district_id`, `created_at`, `updated_at`) VALUES
(1, 'An Đông', 'an-dong', 1, NULL, NULL),
(2, 'An Đông', 'an-dong', 1, NULL, NULL),
(3, 'An Đông', 'an-dong', 1, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_user_id_foreign` (`user_id`),
  ADD KEY `comment_room_id_foreign` (`room_id`);

--
-- Chỉ mục cho bảng `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_province_id_foreign` (`province_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_user_id_foreign` (`user_id`),
  ADD KEY `report_room_id_foreign` (`room_id`);

--
-- Chỉ mục cho bảng `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_user_id_foreign` (`user_id`),
  ADD KEY `room_ward_id_foreign` (`ward_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_ward_id_foreign` (`ward_id`);

--
-- Chỉ mục cho bảng `utility`
--
ALTER TABLE `utility`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ward_district_id_foreign` (`district_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `district`
--
ALTER TABLE `district`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `province`
--
ALTER TABLE `province`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `report`
--
ALTER TABLE `report`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `room`
--
ALTER TABLE `room`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `utility`
--
ALTER TABLE `utility`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `ward`
--
ALTER TABLE `ward`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `district_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `province` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_ward_id_foreign` FOREIGN KEY (`ward_id`) REFERENCES `ward` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ward_id_foreign` FOREIGN KEY (`ward_id`) REFERENCES `ward` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `ward`
--
ALTER TABLE `ward`
  ADD CONSTRAINT `ward_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
